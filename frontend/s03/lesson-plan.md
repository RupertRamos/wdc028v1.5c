# WDC028 - S03 - HTML Introduction

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/19KfZ_aG3nJm8nrH8QuCb7eGIhazEvwydOduUdBNMlSg/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/19KfZ_aG3nJm8nrH8QuCb7eGIhazEvwydOduUdBNMlSg/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s03) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s03/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                 | Link                                                         |
| ------------------------------------- | ------------------------------------------------------------ |
| Interneting Is Hard - Basic Web Pages | [Link](https://www.internetingishard.com/html-and-css/basic-web-pages/) |
| HTML Blocks - Tutorials Point         | [Link](https://www.tutorialspoint.com/html/html_blocks.htm)  |
| HTML Blocks - w3schools               | [Link](https://www.w3schools.com/html/html_blocks.asp)       |
| HTML Elements                         | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Element) |
| Hypertext                             | [Link](https://www.isko.org/cyclo/hypertext)                 |
| em tag vs i tag                       | [Link](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/em) |
| strong vs b tag                       | [Link](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/strong) |
| History of HTML                       | [Link](https://www.w3.org/People/Raggett/book4/ch02.html)    |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [15 mins] - What is HTML? / HTML Tag Structure
	2. [20 mins] - HTML elements
		- html
		- head
		- title
		- body
	3. [10 mins] - Creating a comment
	4. [2 hr 15 mins] - Other HTML elements
		- h1 / block elements
		- h3
		- hr
		- p
		- em / inline elements
		- strong
		- br
		- ul
		- li
		- ol
	5. [1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. The History of HTML

[Back to top](#table-of-contents)