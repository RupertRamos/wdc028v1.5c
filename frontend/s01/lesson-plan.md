# WDC028 - S01 - Bootcamp Introduction and Environment Checks

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |

## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | Link                                                         |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1-oV-sSYq0IipuZnlb3gp4QEP_MZIVDPScY-1VIswgPc/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s01) |
| Manual                  | Link                                                         |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                      | Link                                                         |
| -------------------------- | ------------------------------------------------------------ |
| Account Registration       | [Link](https://docs.google.com/document/d/11t3923URUm4dkJi7k1U3MXsP7ZCCeR5LCF6laCuYSpw/edit) |
| Installation Guide Linux   | [Link](https://docs.google.com/document/d/1Cfyz5qEhZhkNYryxVJAPMUZfJduY07H5PlS-qYO2_QU/edit	) |
| Installation Guide MacOS   | [Link](https://docs.google.com/document/d/1LweZQCrT31wbUp82cjgunBSWtCrZhDM1JcMFAWENFp8/edit#heading=h.gr52vklxtecr) |
| Installation Guide Windows | [Link](https://docs.google.com/document/d/1L7AQw7qwY5BcVT1tbp-QLI7oBom8lhph2fT165QmPbE/edit#heading=h.gr52vklxtecr) |
| Sublime Text Editor 3      | [Link](https://www.sublimetext.com/3)                        |
| Git                        | [Link](https://git-scm.com/downloads)                        |
| GitLab                     | [Link](https://gitlab.com/)                                  |
| GitHub                     | [Link](https://github.com/)                                  |
| Postman                    | [Link](https://www.postman.com/downloads/)                   |
| Heroku                     | [Link](https://devcenter.heroku.com/articles/heroku-cli#download-and-install) |
| MongoDB Community Edition  | [Link](https://www.mongodb.com/try/download/community)       |
| Robo3t                     | [Link](https://studio3t.com/download/?source=robomongo&medium=homepage) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. Sublime Text 3
	2. Git
	3. GitLab/GitHub
	4. Postman
	5. Heroku

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. Terminal Commands to check for installation of applications
	2. How to open the applications

[Back to top](#table-of-contents)

