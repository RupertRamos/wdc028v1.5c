// Note: You can use the snippets here to demonstrate it in the browser console.

// Question: An object's properties can be changed given the following code below:

const person = {
    name: 'Brandon',
    age: 18
}

// Question: What would be the output of the following code block:

let title = 'Lord of the Rings';
console.log(title[2]);

// Question: In the for loop below, what does the (i < 10) part of the code do?

for (let i = 0; i < 10; i++) {
    console.log(i);
}

// Question: Given the following variables, what would be the output?

let conditionA = true;
let conditionB = false;
let conditionC = true;
let result = !(conditionA && (conditionB || conditionC));

console.log(result);
