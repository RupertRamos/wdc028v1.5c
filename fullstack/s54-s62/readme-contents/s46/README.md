# Session Objectives

At the end of the session, the students are expected to:

- learn how to manage data in React.js components by using props and states.

# Resources

## Instructional Materials

- GitLab Repository
- Google Slide Presentation

## Supplemental Materials

- [Components and Props (React.js Docs)](https://reactjs.org/docs/components-and-props.html)
- [Introducing Hooks (React.js Docs)](https://reactjs.org/docs/hooks-intro.html)

# Lesson Proper

## Props

### What is it?

- Props in React.js, short for properties, React.js are the information that a component receives, usually from a parent component.
- Props are synonymous to the function parameters.

### What is it for?

- Props are what allows components to be reusable.
- By using props, one can use the same component and feed different data to the component for rendering.

### Example

A component named **Welcome** is defined via the code below.

```jsx
function Welcome(props) {
    return <h1>Hello, {props.name}</h1>;
}
```

The component receives the data given to it via the props parameter and expects the parameter to be an object with a property of **name**.

In the **index.js**, the Welcome component is used repeatedly to display message for three different names.

```jsx
function App() {
    return (
        <div>
            <Welcome name="John"/>
            <Welcome name="Juan"/>
            <Welcome name="Johan"/>
        </div>
    )
}
```

The output in the browser will have the following text.

```
Hello, John
Hello, Juan
Hello, Johan
```

Props are meant to be read-only and its values must not be changed by the component receiving the information.

If there is information that needs to be changed at any given time within the component, a state must be used instead.

## States

States in React.js allow components to create manage its own data and is meant to be used internally.

Imagine having a traffic light where its states (stop, wait and go) is managed internally by using set intervals (the countdown timer you see before its state changes).

## Hooks

Hooks in React.js are functions that allow developers to create and manage states and lifecycle within components. For states, there is a function called `useState()`.

# Code Discussion

## Setting Up Mock Data

Create a folder named **mock-data** in the React.js project and add a file named **courses.js** inside the created folder.

Then, add the following code inside **courses.js** file.

```jsx
export default [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "Eu non commodo et eu ex incididunt minim aliquip anim. Aliquip voluptate ut velit fugiat laborum. Laborum dolore anim pariatur pariatur commodo minim ut officia mollit ad ipsum ex. Laborum veniam cupidatat veniam minim occaecat veniam deserunt nulla irure. Enim elit sint magna incididunt occaecat in dolor amet dolore consectetur ad mollit. Exercitation sunt occaecat labore irure proident consectetur commodo ad anim ea tempor irure.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "Proident est adipisicing est deserunt cillum dolore. Fugiat incididunt quis aliquip ut aliquip est mollit officia dolor ea cupidatat velit. Consectetur aute velit aute ipsum quis. Eiusmod dolor exercitation dolor mollit duis velit aliquip dolor proident ex exercitation labore cupidatat. Eu aliquip mollit labore do.",
        price: 55000,
        onOffer: true
    },
];
```

## Adding Courses Page

In the **pages** folder, create a file named **Courses.js** and then add the following code.

```jsx
// Base Imports
import React from 'react';
import Course from 'components/Course';

// Bootstrap Components
import Container from 'react-bootstrap/Container';

// Data Imports
import courses from 'mock-data/courses';

export default function Courses() {
    const CourseCards = courses.map((course) => {
        return (
            <Course course={course}/>
        );
    });
    
    return (
        <Container fluid>
            {CourseCards}
        </Container>
    )
}
```

## Displaying Courses Page

Go to **index.js** and add the highlighted code.

```jsx
// Base Imports
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'index.css';

// App Components
import AppNavbar from 'components/AppNavbar';

// Page Components
import Home from 'pages/Home';
import Courses from 'pages/Courses';

ReactDOM.render(
    <Fragment>
        <AppNavbar/>
        <Courses/>
    </Fragment>, 
    document.getElementById('root')
);
```

The output in the page will look like this.

![readme-images/Untitled.png](readme-images/Untitled.png)

There are three course cards but all of them has the same information.

## Using Component Props

To access the course object given from <Course course={course}/>, we need to update the **Course.js** in **components** folder.

```jsx
// Base Imports
import React from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export default function Course(props) {
    let course = props.course;

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>PHP {course.price}</p>
                <Button variant="primary">Enroll</Button>
            </Card.Body>
        </Card>
    )
}
```

Now, the page should look like this.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

## Using PropTypes For Props Validation

To validate whether a component has received the desired props, we can use the PropTypes built-in to React.js.

Go to **course.js** in the **components** folder and add the following:

```jsx
// Base Imports
import React from 'react';
import PropTypes from 'prop-types';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export default function Course(props) {
    let course = props.course;

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>PHP {course.price}</p>
                <Button variant="primary">Enroll</Button>
            </Card.Body>
        </Card>
    )
}

Course.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
};
```

The `PropTypes.shape()` is used to determine whether the props conforms to a specific object "shape". The properties are then defined and the expected property data types are declared along with the setting that the property is required (via the `isRequired`).

To test whether the added code works, go to the **courses.js** in **mock-data** folder and remove the price properties temporarily.

In the Console tab of the DevTools pane, the following warning will show.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

After that, undo the removal of price properties in the **courses.js** file.

Declaring prop types are optional in React.js, though it helps developers see whether a component has received a properly formatted props data.

## Unique Key Props

Another warning is shown in the Console tab of DevTools pane.

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

This warning is shown because when a group of components are dynamically created programmatically (e.g. via an array.map), React.js needs to track these individual components for changes and it is implemented via a unique key prop given to each component.

To fix this warning, go to the **Courses.js** file in **pages** folder and add the highlighted code in the return statement.

```jsx
<Course key={course.id} course={course}/>
```

After saving the changes in the file, the warning should now disappear.

## Creating Enrollee Count State

In the **course.js** of **components** folder, add the following code.

```jsx
export default function Course(props) {
    let course = props.course;

    const [count, setCount] = useState(0);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>PHP {course.price}</p>
                <h6>Enrollees</h6>
                <p>{count} Enrollees</p>
                <Button variant="primary">Enroll</Button>
            </Card.Body>
        </Card>
    )
}
```

The enrollee count will start at zero. The result of the `useState()` function is an array and the contents of the array is then destructured into two distinct identities, the `count` constant and `setCount` function.

To add an enrollee to the count, we will need to increment the count variable when the Enroll button is clicked.

```jsx
export default function Course(props) {
    let course = props.course;

    const [count, setCount] = useState(0);

    function enroll() {
        setCount(count + 1);
    }

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>PHP {course.price}</p>
                <h6>Enrollees</h6>
                <p>{count} Enrollees</p>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
```

We added an `onClick` event handler (which we will discuss more in a few sessions) to the button that will trigger the `enroll()` function. The function then changes the value of the count state by adding +1 to the value of the count.

In React.js, state values must not be changed directly. All changes to the state values must be through the included `setState` function when the state is created.

The output of the added code so far must be like this.

![readme-images/s46-count-state.gif](readme-images/s46-count-state.gif)

The count state is being handled internally in a given course card component. 

# Activity

## Instructions

- Create a seats state in the Course component and set the initial value to 30.
- For every enrollment, deduct one to the seats.
- If the seats reaches zero.
    - Do not add to the count.
    - Do not deduct to the seats.
    - Show an alert that says "no more seats".

## Expected Output

![readme-images/s46-no-more-seats.gif](readme-images/s46-no-more-seats.gif)

## Solution

```jsx
export default function Course(props) {
    let course = props.course;

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(30);

    function enroll() {
        if (seats === 0) {
            alert('No more seats.');
        } else {
            setCount(count + 1);
            setSeats(seats - 1);
        }
    }

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>PHP {course.price}</p>
                <h6>Enrollees</h6>
                <p>{count} Enrollees</p>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
```