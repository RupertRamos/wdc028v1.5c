// Variable to store session number for printing out the grade for the session
let sessionNumber = 26;

const assert = require('assert'); 
const expect = require("chai").expect;
const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s26){

    try {

        // Requires the path to the index file inside the activity folder
        const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);

        // Variable to store passing unit test scores for the session
        let passingTests = 0;
        // Variable to store total number of session unit tests
        let totalTests = 0;

        describe(`s${sessionNumber}`, function () {

            let startTime;
            let users = activity.users;
            let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

                if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})



            // Unit tests for the activity
            it('should add a new value inside the array', () => {
                // Set the current length of the array to a variable
                let current_length = users.length

                // Adds a new item
                activity.addItem("Triple H")

                // Compares the length of array after adding an item to the length of it before adding an item
               expect(users.length).above(current_length);
            })

            it('should get a single array item based on index', () => {
                // Checks if the item returned from the function exists in the arrayL
               expect(users).to.include(activity.getItemByIndex(users.length - 1));
            })

            // fix
            it('deleteItem function must return last item in array', () => {
                // Checks if the item returned from the function exists in the array

                let itemtoDelete = users[users.length - 1];

                expect(itemtoDelete).to.equal(activity.deleteItem());
            })

            it('users array length must be lower', () => {

                activity.addItem("Triple H")
                
                let current_length = users.length

                activity.deleteItem()

                let updated_length = users.length 

               expect(updated_length).to.be.below(current_length);
            })

            it('should update a single item by its index number', () => {
                let before_update_item = users[users.length - 1]

                activity.updateItemByIndex("Test", users.length - 1)

               expect(users[users.length - 1]).to.be.not.equal(before_update_item);
            })

            it('should delete all items in the array', () => {
                activity.deleteAll()
               expect(users.length).to.equal(0);
            })

            it('should return true or false depending on the array', () => {''
               expect(activity.isEmpty()).to.be.ok;
            })

            // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
            // Use of objects with the "session number" as keys to allow for ease of automation
            after(function () {

                const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);
                
				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

                jsonData.gradedTest.s26 = true;
                fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
            });

        });
        
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {
    console.log(`S${sessionNumber} - Tested`);
}


