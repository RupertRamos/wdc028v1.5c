let sessionNumber = 45;
const chai = require('chai');
// Chai http package allows for use of request method to process HTTP requests on API
const http = require('chai-http');
// Stores expect method from chai module to use for sending HTTP requests
const expect = chai.expect;
// Allows use of chai methods like "request", "post", etc.
chai.use(http);

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s45){

	try {

		const {app,mongoose} = require("../sessions/backend/s43-s48/index");

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		describe(`s45`, function () {
			this.timeout(30000);
            let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
				if (this.currentTest.state === 'failed') {
                    errors.push({
                        test: this.currentTest.title,
                        feedback: this.currentTest.err.message
                    });
                }
			})

			// Test to check if route implements jwt authentication for admin on successful post request
			it("test_api_create_course_is_successful", (done) => {

				// The "request" method accepts base url for the request
				// The "post" method accepts the specified route to be added on top of the base url
				// The "set" method allows to add http header data. The token provided below is for an admin user
				// The "type" method defines the type of data to be used for the request
				// The "send" method contains the body of the request object to be sent
				// The "end" method is used to add the different test assertions/expectations from the request
				// The "res" object has access to a "body" property which contains the return value of the API
				chai.request(app)
				.post('/courses/')
				.set('authorization', "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxM2U5OTY5MWRiZTA2MjlmOGQwM2JjMyIsImVtYWlsIjoiYWRtaW5AbWFpbC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2NzQ1ODMzMTd9.ey4aZuq0xlpbWm6WgxzEPAIkj4x_-usZOVKb-oo8Op4")
				.type('json')
				.send({
					name: "HTML",
					description: "Learn the fundamentals of programming.",
					price: 1000
				})
				.end((err, res) => {
					expect(res.body).to.equal(true);
					done();
				})

			})

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(async function () {
				
				await mongoose.connection.close()
				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);
				
				global.gradesObject["s45"] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
                    feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s45 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {
	console.log(`S${sessionNumber} - Tested`);
}



