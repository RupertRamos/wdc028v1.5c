let sessionNumber = 44;
// const mongoose = require("mongoose");
const chai = require('chai');
// Chai http package allows for use of request method to process HTTP requests on API
const http = require('chai-http');
// Stores expect method from chai module to use for sending HTTP requests
const expect = chai.expect;
// Allows use of chai methods like "request", "post", etc.
chai.use(http);

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s44){

	try {
		// Requires the path to the index file inside the activity folder
		const {app,mongoose} = require("../sessions/backend/s43-s48/index");

		// const testDatabaseUri = "mongodb+srv://admin:admin123@cluster0.zlyew.mongodb.net/bookingAPI?retryWrites=true&w=majority";
		// connectToDatabase(testDatabaseUri);

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		describe(`s44`, function () {

			this.timeout(30000);

            let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			  await mongoose.connection.close()

			 await mongoose.connect("mongodb+srv://admin:1234@cluster0.zlyew.mongodb.net/bookingAPI?retryWrites=true&w=majority", {
				useNewUrlParser : true,
				useUnifiedTopology : true
			});


			await mongoose.connection.once("open", () => console.log(`Now connected to MongoDB Atlas in Mocha Test`));
			  
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;


			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

				if (this.currentTest.state === 'failed') {
                    errors.push({
                        test: this.currentTest.title,
                        feedback: this.currentTest.err.message
                    });
                }
			})

			// Test to check whether the provided jwt returns an object of the user schema with the following properties
			it("test_api_get_user_detail", (done)=>{

				// The "request" method accepts base url for the request
				// The "get" method accepts the specified route to be added on top of the base url
				// The "set" method allows to add http header data. The token provided below is for a non-admin user
				// The "end" method is used to add the different test assertions/expectations from the request
				// The "res" object has access to a "body" property which contains the return value of the API
				chai.request(app)
				.post('/users/details')
				.type('json')
				.send({
					id: "64191ccaf38abc03d4127c90"
				})
				.end((err, res) => {
					// Checks if the response body contains an object with all the provided properties
					expect(res.body,"response body returned does not have all instructed properties").to.include.all.keys('firstName', 'lastName', 'email', 'password', 'mobileNo', 'enrollments', 'isAdmin')
					done();
				})

			})

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(async function () {
			
				await mongoose.connection.close()
				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject["s44"] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
                    feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s44 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		//   console.log(err)
		}
	}

} else {
	console.log(`S${sessionNumber} - Tested`);
}


