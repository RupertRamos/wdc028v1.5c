// Variable to store session number for printing out the grade for the session
let sessionNumber = 40;

const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s40){

	try {

		// Requires the path to the index file inside the activity folder
		const app = require(`../sessions/backend/s${sessionNumber}/index`)

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		describe(`s${sessionNumber}`, function () {

            let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
				if (this.currentTest.state === 'failed') {
                    errors.push({
                        test: this.currentTest.title,
                        feedback: this.currentTest.err.message
                    });
                }
			})

			it("test_api_get_home_is_running", ()=>{

				let result = chai.request(app)
				.get('/home')
				.end((err, res) => {

					expect(res.text.toLowerCase(),"response text does not include welcome").to.include("welcome");

				})

			})

			// used to add a user to test if users array returns array of objects
			it("test_api_post_signup_is_running", (done)=>{
				chai.request(app)
				.post('/register')
				.type('json')
				.send({
					username: "johndoe",
					password: "johndoe123"
				})
				.end((err, res) => {
					expect(res.status,"response status should be equal to 200").to.equal(200);
					done(err);
				})
			})

			// /users route
			it("test_api_get_users_is_running", ()=>{

				chai.request(app)
				.get('/users')
				.end((err, res) => {
					expect(res.body[0],"response body is missing a property").to.have.all.keys('username', 'password')
				})
			})

			// checking if the user will be deleted if the usernames don't match
			it("test_api_delete-user_non-existing_user_is_running", (done)=>{

				

				chai.request(app)
				.post('/register')
				.type('json')
				.send({
					username: "johndoe",
					password: "johndoe123"
				})
				.end((err, res) => {
					if(res.status === 200){

						chai.request(app)
						.delete('/delete-user')
						.type('json')
						.send({
							username: "john",
							password: "johndoe123"
						})
						.end((err, res) => {
							expect(res.text,"response text does not include 'does not exist'").to.include(`does not exist`);
							done();
						})

					} else {
						expect(res.status,"response status should be equal to 200 cannot signup").to.equal(200);
						done();
					}
				})

			})

			// checking if an existing user will be deleted
			it("test_api_get_delete-user_is_running", (done)=>{
				chai.request(app)
				.post('/register')
				.type('json')
				.send({
					username: "johndoe",
					password: "johndoe123"
				})
				.end((err, res) => {

					if(res.status === 200){

						chai.request(app)
						.delete('/delete-user')
						.type('json')
						.send({
							username: "johndoe",
							password: "johndoe123"
						})
						.end((err, res) => {
							expect(res.text,"response text does not include 'has been deleted'").to.include(`has been deleted`);
							done();
						})

					} else {
						expect(res.status,"response status should be equal to 200 cannot signup").to.equal(200);
						done();
					}

				})
			})

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);
				
				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
                    feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s40 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {
	console.log(`S${sessionNumber} - Tested`);
}



