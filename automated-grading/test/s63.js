// Variable to store session number for printing out the grade for the session
let sessionNumber = 63;

const assert = require('assert'); 
const expect = require("chai").expect;
const fs = require('fs');

const { performance } = require('perf_hooks');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s63){

	try {
		// Requires the path to the index file inside the activity folder
		const source = require(`../s${sessionNumber}/activity/index`);


		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;



		describe(`s${sessionNumber}`, function () {

			let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})

			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

				// If there are any errors, save them to the array
				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
				
			})


			it('[1] Count letters in a sentence.', function() {
				const validLetter = 'o';
				const invalidLetter = 'abc';
				const sentence = 'The quick brown fox jumps over the lazy dog';
				const result = source.countLetter(invalidLetter, sentence);
				expect(result,"Invalid letter does not return undefined").to.be.undefined;

				const result2 = source.countLetter(validLetter, sentence);
				expect(result2,"countLetter() does not return correct number").to.equal(4);
			});

			it('[2] Check isogram.', function() {
				const isogram = 'Machine';
				const notIsogram = 'Hello';

				const result = source.isIsogram(isogram);
				expect(result,"isIsogram does not return true when given an isogram").to.equal(true);

				const result2 = source.isIsogram(notIsogram);
				expect(result2,"isIsogram does not return false when given an non-isogram").to.equal(false);
			
			});
			
			it('[3] Purchase goods.', function() {
				const price = 109.4356;
				const discountedPrice = price * 0.8;
				const roundedPrice = discountedPrice.toFixed(2);

				const result = source.purchase(12, price);
				expect(result,"Does not returns undefined for students aged below 13.").to.be.undefined;

				const result2 = source.purchase(15, price);
				expect(result2,"Returns discounted price (rounded off) for students aged 13 to 21.").to.equal(roundedPrice);

				const result3 = source.purchase(72, price);
				expect(result3,'Returns discounted price (rounded off) for senior citizens.').to.equal(roundedPrice);

				const result4 = source.purchase(34, price);
				expect(result4,'Returns price (rounded off) for people aged 22 to 64.').to.equal(price.toFixed(2));
			});
			
			it('[4] Find hot categories.', function() {
				const items = [
					{ id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
					{ id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
					{ id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
					{ id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
					{ id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
				];
				const result = source.findHotCategories(items);
				expect(result,'Does not return item categories without stocks.').to.deep.equal(['toiletries', 'gadgets']);

			});
			
			it('[5] Find flying voters.', function() {
				const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
				const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

				const result = source.findFlyingVoters(candidateA, candidateB);
				expect(result,'Does not return the array of flying voters.').to.deep.equal(['LIWf1l', 'V2hjZH']);
			
			});
			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}


				jsonData.gradedTest.s63 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
			console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
			console.log(err)
		}
	}
} else {
	console.log("S63 - Tested");
}
