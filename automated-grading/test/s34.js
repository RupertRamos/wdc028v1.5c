//Sample
let sessionNumber = 34;

const { MongoClient } = require('mongodb');
const chai = require('chai');
const expect = chai.expect;

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s34){

    try {

		// Requires the path to the index file inside the activity folder
		const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);
		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		describe(`s${sessionNumber}`, function () {

            let client;
            let db;
            this.timeout(50000)
            let startTime;
            let errors = [];
			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
                if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})
            
            before(async () => {
                startTime = performance.now();
                // Connect to local MongoDB server
                client = await MongoClient.connect('mongodb+srv://admin:admin1234@cluster0.zlyew.mongodb.net/bookingAPI?retryWrites=true&w=majority', { useUnifiedTopology: true });
                db = client.db('hotel');
                db.rooms = db.collection('rooms')
            
            });
       
            it('should retrieve a "single" room document with expected values', async () => {
                // Retrieve a room document with the specified name
                
                const room = await activity.addOneFunc(db).then((result) => 
                // console.log(result)
                result.collection('rooms').findOne({ name: 'single' })
            );
                // console.log(room)
                // Check if the retrieved room document has the expected values
                expect(room,"Room name single cannot be found").to.exist;
                expect(room.name).to.equal('single',"Room name single cannot be found");
                expect(room.accommodates).to.equal(2,"Room accomodates property not equal to 2");
                expect(room.price).to.equal(1000,"Room price not equal to 1000");
                expect(room.description.toLowerCase()).to.include('simple room',"Room description does not match instructed value");
                expect(room.rooms_available).to.equal(10,"rooms_available value not equal to 10");
                expect(room.isAvailable,"room isAvailable property is true.").to.be.false;
            });
            
            it('should create double and queen room document with expected values', async () => {
                // Retrieve a room document with the specified name
                
                const rooms = await activity.addManyFunc(db).then((result) => 
                // console.log(result)
                result.collection('rooms').find({ 
                    $or: [
                        {name: "double"},
                        {name: "queen"}
                        ]
                }).toArray()
            );
                
                // console.log(rooms)
                rooms.forEach((room) => {
            
                // Check if the retrieved room document has the expected values
                expect(room).to.exist;
            
                expect(room.name).to.satisfy(function () {
                    if ((room.name.toLowerCase() === 'double') || (room.name.toLowerCase()  === 'queen')) {
                        return true;
                    } else {
                        return false;
                    }
                },"Double or Queen room does not exist.");
            
                expect(room.accommodates).to.satisfy(function () {
                    if ((room.accommodates === 3) || (room.accommodates === 4)) {
                        return true;
                    } else {
                        return false;
                    }
                },"Room accomodation is not 3 or 4");
            
                expect(room.price).to.satisfy(function () {
                    if ((room.price === 2000) || (room.price === 4000)) {
                        return true;
                    } else {
                        return false;
                    }
                },"Room prices is not 2000 or 4000");
            
                expect(room.description).to.satisfy(function () {
                    if ((room.description === 'A room fit for a small family going on a vacation') || (room.description === 'A room with a queen sized bed perfect for a simple getaway')) {
                        return true;
                    } else {
                        return false;
                    }
                },"Room Descriptions don't match instructed value");
            
                expect(room.rooms_available).to.satisfy(function () {
                    if ((room.rooms_available === 5) || (room.rooms_available === 15)) {
                        return true;
                    } else {
                        return false;
                    }
                },"Rooms available is not 5 or 15");
            
                expect(room.isAvailable).to.satisfy(function () {
                    if (room.isAvailable === false) {
                        return true;
                    } else {
                        return false;
                    }
            
                },"One of the roms are marked as available.");
                
                })
                
            });
            
            it('should retrieve the "double" room document with expected values', async () => {
                // Retrieve a room document with the specified name
                
                let room = await activity.findRoom(db).then(result => result);
                // Check if the retrieved room document has the expected values
                expect(room,"Room cannot be found with query given.").to.exist
                expect(room.name).to.equal('double',"Room name is not double")
                expect(room.accommodates).to.equal(3,"Room accomodates property is not equal to 3")
                expect(room.price).to.equal(2000,"Room price property is not equal to 2000")
                expect(room.description).to.equal("A room fit for a small family going on a vacation","Room description property does not match instructions")
                expect(room.rooms_available).to.equal(5,"Room available property is not equal to 5")
                expect(room.isAvailable,"Room isAvailable property is not equal to false").to.be.false
            })
            
            
            it('should retrieve the updated "queen" room document with expected values', async () => {
                // Retrieve a room document with the specified name
                await activity.updateOneFunc(db)
                
                const room = await db.collection('rooms').findOne({name: "queen"});
                console.log(room)
                // Check if the retrieved room document has the expected values
                expect(room,"Room cannot be found with query given.").to.exist;
                expect(room.name).to.equal('queen',"Room name is not queen");
                expect(room.accommodates).to.equal(4,"Room accomodates property is not equal to 4");
                expect(room.price).to.equal(4000,"Room price property is not equal to 4000");
                expect(room.description).to.equal("A room with a queen sized bed perfect for a simple getaway","Room description property does not match instructions");
                expect(room.rooms_available).to.equal(0,"Room available property is not equal to 0");
                expect(room.isAvailable,"Room isAvailable property is not equal to false").to.be.false;
            })
            
            it('should query to the room collection to check if queen room has been deleted.', async () => {
                // Retrieve a room document with the specified name
                await activity.deleteManyFunc(db)
                const rooms = await db.collection('rooms').find({});
            
                rooms.forEach((room) => {
            
                expect(room.rooms_available).to.not.equal(0,"A room document with 0 roomsAvailable still exists")
            
                });
            
            })

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(async function () {

                const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

                //Drop Database after use
                await db.dropDatabase()

                // Disconnect from MongoDB server
                await client.close();

				jsonData.gradedTest.s34 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});

        
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
    console.log(`S${sessionNumber} - Tested`);
}
