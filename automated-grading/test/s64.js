// Variable to store session number for printing out the grade for the session
let sessionNumber = 64;

const assert = require('assert'); 
const expect = require("chai").expect;
const fs = require('fs');

const { performance } = require('perf_hooks');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s64){

	try {
		// Requires the path to the index file inside the activity folder
		const queue = require(`../s${sessionNumber}/activity/queue`);


		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;



		describe(`s${sessionNumber}`, function () {

			let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})

			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

				// If there are any errors, save them to the array
				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
				
			})


			it('[1] Print queue elements.', function() {
				expect(queue.print(),"The printed value is not an empty array. RESULT = []").to.be.empty;
			});
			
			it('[2] Enqueue a new element.', function() {
				expect(queue.enqueue('John'),"The value is not enqueued. RESULT = [\'John\']").to.deep.equal(['John']);
			});
			
			it('[3] Enqueue another element.', function() {
				expect(queue.enqueue('Jane'),'The value is not enqueued. RESULT = [\'John\', \'Jane\']').to.deep.equal(['John', 'Jane']);
			});
			
			it('[4] Dequeue the first element.', function() {
				expect(queue.dequeue(),'The value has not been dequeued. RESULT = [\'Jane\']').to.deep.equal(['Jane']);
			});
			
			it('[5] Enqueue another element.', function() {
				expect(queue.enqueue('Bob'),'The value has not been enqueued. RESULT = [\'Jane\', \'Bob\']').to.deep.equal(['Jane', 'Bob']);
			});
			
			it('[6] Enqueue another element.', function() {
				expect(queue.enqueue('Cherry'),'The value has not been enqueued. RESULT = [\'Jane\', \'Bob\', \'Cherry\']').to.deep.equal(['Jane', 'Bob', 'Cherry']);
			});
			
			it('[7] Get first element.', function() {
				expect(queue.front(),'The first value has not been retrieved. RESULT = \'Jane\'').to.equal('Jane');
			});
			
			it('[8] Get queue size.', function() {
				expect(queue.size(),'The size of the queue has not been retrieved. RESULT = 3').to.equal(3);
			});
			
			it('[9] Check if queue is not empty.', function() {
				expect(queue.isEmpty(),'The result has not been retrieved. RESULT = false').to.equal(false);
			});
			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}


				jsonData.gradedTest.s64 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
			console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
			console.log(err)
		}
	}
} else {
	console.log("S64 - Tested");
}
