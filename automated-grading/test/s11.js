let sessionNumber = 11;
require('geckodriver');
// Generated by Selenium IDE
const { Builder, By, Key, until } = require('selenium-webdriver')
const firefox = require('selenium-webdriver/firefox');
const { performance } = require('perf_hooks');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const chai = require('chai');
const expect = chai.expect;

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(jsonData.gradedTest.s11 === false){

	try {

    if (!fs.existsSync('./sessions/frontend/s11/activity/index.html')) {
			throw new Error('File not found');
		  }
      //Automated Grading Integration
      // Variable to store passing unit test scores for the session
      let passingTests = 0;
      // Variable to store total number of session unit tests
      let totalTests = 0;


      function parsePixels(value) {
          return parseFloat(value.replace('px', ''));
        }

      describe('s11', function() {

        this.timeout(30000);
        const timeout = 10000;
        let driver;
        let vars;

        let internalCssTestPassed = false;

        let startTime;
        let errors = [];
        before(async function() {
          startTime = performance.now();
          // Any setup code you need to run before the tests start
          driver = await new Builder().forBrowser('firefox').setFirefoxOptions(options).build()
        });
        
        const options = new firefox.Options();
        options.headless();
  
        options.setPreference('browser.download.manager.showWhenStarting', false);
        options.setPreference('browser.download.manager.useWindow', false);
  
        beforeEach(async function() {

          totalTests += 1;
          vars = {}
        })
  
        afterEach(function(){
  
          if(this.currentTest.state === "passed"){
            passingTests += 1;
          }
  
          process.removeAllListeners(); 
          
          if (this.currentTest.state === 'failed') {
            errors.push({
                test: this.currentTest.title,
                feedback: this.currentTest.err.message
            });
        }
        })

        it('Check if the page has bootstrap version 4.6', async function() {

            const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
            const url = `file://${filePath}`;
            await driver.get(url);

            const bootstrapVersion = await driver.findElement(By.css("link")).getAttribute("href");
            expect(bootstrapVersion,"bootstrap version is incorrect").to.include("4.6");

        });

        it("should have a navbar element and a Bootstrap class to always be at the top, the text alignment at center, the bg is primary", async () => {
            const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
            const url = `file://${filePath}`;
            await driver.get(url);

            const navbar = await driver.findElement(By.id("navbar"));
            const navbarBG = await navbar.getCssValue("background-color");
            expect(navbarBG).to.equal("rgb(0, 123, 255)","Element with id navbar does not have bg-primary");
            
            const navbarClass = await navbar.getAttribute("class");
            expect(navbarClass).to.satisfy((navClass)=>{
              if(navClass.includes("sticky-top")|| navClass.includes("fixed-top")){
                return true
              }
            },"navbar is not sticky or fixed at top");

            const navbarAlign = await navbar.getCssValue("text-align");
            expect(navbarAlign).to.equal("center","navbar is not text-aligned as center");

            const navbarPadding = parsePixels(await navbar.getCssValue("padding"));
            expect(navbarPadding).to.be.at.least(4,"navbar does not have padding.");

        });

        it("should have navbar links without text decoration", async () => {
            const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
            const url = `file://${filePath}`;
            await driver.get(url);

            const navbarLinks = await driver.findElements(By.css("#navbar > a"));
            for (const link of navbarLinks) {
              const textDecoration = await link.getAttribute("class");
              expect(textDecoration).to.include("text-decoration-none","navbar links has no text-decoration-none class");
            }

        });

        it("div with id about-me should have text-center, mt-5", async () => {
            const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
            const url = `file://${filePath}`;
            await driver.get(url);

            const aboutMe = await driver.findElement(By.css("#about-me"));
            const divAlign = await aboutMe.getCssValue("text-align");
            expect(divAlign).to.equal("center","div is not text-aligned as center");

            const divMarginTop = parsePixels(await aboutMe.getCssValue("margin-top"));
            expect(divMarginTop).to.be.at.least(1,"about me section does not have margin top.")

        });

        it("div with id profile should have mx-auto", async () => {
          const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
          const url = `file://${filePath}`;
          await driver.get(url);

          const profile = await driver.findElement(By.css("#profile"));
          const profileCenter = await profile.getAttribute("class");
          expect(profileCenter).to.satisfy((profileCenter) => {
            return profileCenter.includes("ml-auto") && profileCenter.includes("mr-auto") ||
            profileCenter.includes("mx-auto");
          },"profile is not centered using auto margins.");

      });

        it("h2 inside the about-me section should have primary bottom border", async () => {
            const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
            const url = `file://${filePath}`;
            await driver.get(url);

            const h2Child = await driver.findElement(By.css("#about-me > div h2"));
            const h2BottomBorderStyle = await h2Child.getCssValue("border-bottom-style");
            const h2BottomBorderWidth = await h2Child.getCssValue("border-bottom-width");
            const h2BottomBorderColor = await h2Child.getCssValue("border-bottom-color");
            
            expect(h2BottomBorderWidth,"no bottom border").to.equal("1px");
            expect(h2BottomBorderStyle,"border style must be solid").to.equal("solid");
            expect(h2BottomBorderColor,"border bottom color is not primary").to.equal("rgb(0, 123, 255)");
            
        });

        it("img with id ada is rounded and has img-fluid", async () => {
            const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
            const url = `file://${filePath}`;
            await driver.get(url);

            const imgChild = await driver.findElement(By.css("#about-me > div > img"));
            const imgRadius = await imgChild.getCssValue("border-radius");
            expect(imgRadius,"img radius is not 50%").to.equal("50%");

            const imgFluid = await imgChild.getAttribute("class");
            expect(imgFluid,"img does not have img-fluid").to.include("img-fluid");

        });

        it("paragraph inside about me should be at least have and if it has automatic left and right margins", async () => {
            const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
            const url = `file://${filePath}`;
            await driver.get(url);

            const pChild = await driver.findElement(By.css("#about-me > p"));
            const classes = await pChild.getAttribute("class");

            expect(classes).to.satisfy((classes) => {
                return classes.includes("ml-auto") && classes.includes("mr-auto") ||
                  classes.includes("mx-auto");
              },"paragraph inside about me is not centered using auto margins.");

        });

        it("div id projects has a secondary bg, text-center and paddings", async () => {
          const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
          const url = `file://${filePath}`;
          await driver.get(url);

          const divProjects = await driver.findElement(By.css("#projects"));
          const bgColor = await divProjects.getAttribute("class");
          const textAlign = await divProjects.getCssValue("text-align");
          const paddingTop = await divProjects.getCssValue("padding-top").then(parsePixels);
          const paddingBottom = await divProjects.getCssValue("padding-bottom").then(parsePixels);
          chai.expect(bgColor,"bg set is not secondary").to.include("bg-secondary");
          chai.expect(textAlign,"text alignment is not center").to.equal("center");
          chai.expect(paddingTop,"no padding top").to.be.at.least(1);
          chai.expect(paddingBottom,"no padding bottom").to.be.at.least(1);
        });

        it("should check if the divs inside the div with id 'projects' have d-inline-block, and text-light", async () => {
          const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
          const url = `file://${filePath}`;
          await driver.get(url);
          
          const element = await driver.findElement(By.id("projects"));
          const childElements = await element.findElements(By.css("div"));
          for (let i = 0; i < childElements.length; i++) {
            const radius = await childElements[i].getCssValue("border-radius");
            const displayValue = await childElements[i].getCssValue("display");
            const colorValue = await childElements[i].getCssValue("color");
            const bgValue = await childElements[i].getCssValue("background-color");
            const padding = await childElements[i].getCssValue("padding");
            const marginLeft = await childElements[i].getCssValue("margin-left");
            const marginRight = await childElements[i].getCssValue("margin-left");
            expect(displayValue,"divs are not inline block").to.equal("inline-block");
            expect(colorValue).to.satisfy((colorValue)=> colorValue.includes("rgb(248, 249, 250)") || colorValue.includes("rgb(255, 255, 255)"),"text color is not instructed color");
            expect(parsePixels(radius)).to.be.at.least(4,"A project card has No border-radius");
            expect(bgValue).to.include("rgb(52, 58, 64)","project div bg color is not dark");
            expect(parsePixels(padding)).to.be.at.least(4,"Project divs does not have paddings.");
            expect(parsePixels(marginLeft)).to.be.at.least(4,"Project divs does not left margin.");
            expect(parsePixels(marginRight)).to.be.at.least(4,"Project divs does not right margin.");;
          }
        });
        
        it("should check if the div with id footer has bg primary and has p-3 and text-light", async () => {
          const filePath = path.join(__dirname, '..','sessions','frontend','s11','activity','index.html');
          const url = `file://${filePath}`;
          await driver.get(url);
          
          const footer = await driver.findElement(By.css("#footer"));
          const footerBG = await footer.getCssValue("background-color");
          expect(footerBG).to.equal("rgb(0, 123, 255)","Element with id footer does not have bg-primary");
        

          const footerAlign = await footer.getCssValue("text-align");
          expect(footerAlign).to.equal("center","footer is not text-aligned as center");

          const colorValue = await driver.findElement(By.css("#footer > p")).getCssValue("color");
          expect(colorValue).to.satisfy((colorValue)=> colorValue.includes("rgb(248, 249, 250)") || colorValue.includes("rgb(255, 255, 255)"));

          const paddingTop = await footer.getCssValue("padding-top");
          const paddingBottom = await footer.getCssValue("padding-bottom");
          chai.expect(paddingTop,"padding top is not equal to 1rem").to.equal("16px");
          chai.expect(paddingBottom,"padding bottom is not equal to 1rem").to.equal("16px");

        });

        after(async function () {

          
          const endTime = performance.now();
          const totalTime = (endTime - startTime) / 1000; // Convert to seconds
          console.log(`Total time taken: ${totalTime} seconds`);
          
          global.gradesObject["s11"] = {
            grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
            score: `${passingTests}/${totalTests}`,
            feedback: errors.length ? errors : "No Errors"
          }
          
          jsonData.gradedTest.s11 = true;
          await fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
          await driver.quit(); 
        });

      })	

	
    } catch (err) {
      if(err.code === "MODULE_NOT_FOUND" || err.message === "File not found"){
        console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
      } else {
        console.log(err)
      }
    }
} else {
	console.log("s11 - Tested");
}






