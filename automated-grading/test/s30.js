// Variable to store session number for printing out the grade for the session
let sessionNumber = 30;

const assert = require('assert'); 
const expect = require("chai").expect;
const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s30){

    try {

        // Requires the path to the index file inside the activity folder
        const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);

        // Variable to store passing unit test scores for the session
        let passingTests = 0;
        // Variable to store total number of session unit tests
        let totalTests = 0;

        describe(`s${sessionNumber}`, function () {

            let startTime;
            let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
                if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})


            it('should get the cube of 2', () => {
                expect(activity.getCube).to.equal(8,"getCube variable is not equal to 8");
            })

            it('should get values in the array by destructuring', () => {
                expect(activity.houseNumber,"houseNumber element was not properly destructured from array as a variable").to.not.be.undefined;
                expect(activity.street,"street element was not properly destructured from array as a variable").to.not.be.undefined;
                expect(activity.state,"state element was not properly destructured from array as a variable").to.not.be.undefined;
                expect(activity.zipCode,"zipCode element was not properly destructured from array as a variable").to.not.be.undefined;
            })

            it('should get values in the object by destructuring', () => {
                expect(activity.name,"name property was not properly destructured from array as a variable").to.not.be.undefined;
                expect(activity.species,"species property was not properly destructured from array as a variable").to.not.be.undefined;
                expect(activity.weight,"weight property was not properly destructured from array as a variable").to.not.be.undefined;
                expect(activity.measurement,"measurement property was not properly destructured from array as a variable").to.not.be.undefined;
            })

            it('should output the result of the arrow function', () => {
                expect(activity.reduceNumber).to.equal(15,"number not reduced properly to 15");
                // expect().notEqual(activity.reduceNumber, null)
            })

            it('should be able to create a new instance of Dog object', () => {
                const myDog = new activity.Dog("Scooby", 8, "Rottweiler");

                expect(myDog,"dog instance does not have name property").to.have.property('name')
                expect(myDog,"dog instance does not have age property").to.have.property('age')
                expect(myDog,"dog instance does not have breed property").to.have.property('breed')
            })

            // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
            // Use of objects with the "session number" as keys to allow for ease of automation
            after(function () {
                
                const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

                jsonData.gradedTest.s30 = true;
                fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
            });

        });
        
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
    console.log(`S${sessionNumber} - Tested`);
}


