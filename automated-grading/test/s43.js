let sessionNumber = 43;

const chai = require('chai');
// Chai http package allows for use of request method to process HTTP requests on API
const http = require('chai-http');
// Stores expect method from chai module to use for sending HTTP requests
const expect = chai.expect;
// Allows use of chai methods like "request", "post", etc.
chai.use(http);
const mongoose = require('mongoose');

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s43){

	try {
		// Requires the path to the index file inside the activity folder
		const userModel = require("../sessions/backend/s43-s48/models/User.js");

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		describe(`s43`, function () {

            let startTime;
            let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
                if (this.currentTest.state === 'failed') {
                    errors.push({
                        test: this.currentTest.title,
                        feedback: this.currentTest.err.message
                    });
                }
			})

              // Test for firstName property

            it('should have a firstName property of type string.', () => {
                const user = new userModel({
                    firstName: "John"
                }); // Instantiate userModel
                expect(user,"model does not have firstName property").to.have.property('firstName');
                expect(user.firstName).to.be.a("string","firstName property is not a string")
            });

            it('should have a required firstName property', () => {
                const user = new userModel(); // Instantiate userModel
                const error = user.validateSync(); // Validate the model
                expect(error.errors.firstName.kind).to.equal('required',"property is not required."); // Check that the validation error is of kind 'required'
            });

            // Test for lastName property
            it('should have a lastName property of type string', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe"
                }); // Instantiate userModel
                expect(user,"model does not have lastName property").to.have.property('lastName');
                expect(user.lastName).to.be.a("string","lastName property is not a string")
            });

            it('should have a required lastName property', () => {
                const user = new userModel(); // Instantiate userModel
                const error = user.validateSync(); // Validate the model
                expect(error.errors.lastName.kind).to.equal('required',"property is not required."); // Check that the validation error is of kind 'required'
            });

            // Test for email property
            it('should have a email property of type string', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe",
                    email: "jdoe2003@gmail.com"
                }); // Instantiate userModel
                expect(user,"model does not have email property").to.have.property('email',);
                expect(user.email).to.be.a("string","email property is not a string")
            });

            it('should have a required email property', () => {
                const user = new userModel(); // Instantiate userModel
                const error = user.validateSync(); // Validate the model
                expect(error.errors.email.kind).to.equal('required',"property is not required."); // Check that the validation error is of kind 'required'
            });

            // Test for password property
            it('should have a password property of type string', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe",
                    email: "jdoe2003@gmail.com",
                    password: "1234567"
                }); // Instantiate userModel
                expect(user,"model does not have password property").to.have.property('password');
                expect(user.password).to.be.a("string","password property is not a string");
            });

            it('should have a required password property', () => {
                const user = new userModel(); // Instantiate userModel
                const error = user.validateSync(); // Validate the model
                expect(error.errors.password.kind).to.equal('required',"property is not required."); // Check that the validation error is of kind 'required'
            });

            // Test for mobileNo property
            it('should have a mobileNo property of type string', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe",
                    email: "jdoe2003@gmail.com",
                    password: "1234567",
                    mobileNo: "09261234567"
                }); // Instantiate userModel
                expect(user,"model does not have mobileNo property").to.have.property('mobileNo');
                expect(user.mobileNo).to.be.a("string","mobileNo property is not a string");
            });

            it('should have a required mobileNo property', () => {
                const user = new userModel(); // Instantiate userModel
                const error = user.validateSync(); // Validate the model
                expect(error.errors.mobileNo.kind).to.equal('required',"property is not required."); // Check that the validation error is of kind 'required'
            });

            // Test for isAdmin property
            it('should have a isAdmin property', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe",
                    email: "jdoe2003@gmail.com",
                    password: "1234567"
                }); // Instantiate userModel
                expect(user,"model does not have isAdmin property").to.have.property('isAdmin');
            });

            it('should have a default isAdmin property of false', () => {
                const user = new userModel(); // Instantiate userModel
                expect(user.isAdmin,"isAdmin does not have default false value.").to.be.false; // Check that the default value of isAdmin is false
            });

            // Test for enrollments property
            it('should have a enrollments property', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe",
                    email: "jdoe2003@gmail.com",
                    password: "1234567",
                    
                }); // Instantiate userModel
                expect(user,"model does not have enrollments property").to.have.property('enrollments');
            });

            it('should have an enrollments property of type array', () => {
                const user = new userModel(); // Instantiate userModel
                expect(user.enrollments).to.be.an('array',"enrollments may not be an array"); // Check that the enrollments property is of type array
            });

            // Test for courseId property within enrollments
            it('should have a courseId property', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe",
                    email: "jdoe2003@gmail.com",
                    password: "1234567",
                    enrollments: [{
                        courseId: "sampleId-1"
                    }]
                }); // Instantiate userModel
                expect(user.enrollments[0],"model does not have courseId property in subdocument").to.have.property('courseId');
            });

            it('should have a required courseId property within enrollments', () => {
                const user = new userModel({ // Instantiate userModel with default enrollments
                enrollments: [{
                    enrolledOn: new Date()
                }]
                });
                const error = user.validateSync(); // Validate the model
                expect(error.errors['enrollments.0.courseId'].kind,"enrolledOn must be required.").to.equal('required'); // Check that the validation error is of kind 'required'
            });

            // Test for enrolledOn property within enrollments
            it('should have a enrolledOn property', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe",
                    email: "jdoe2003@gmail.com",
                    password: "1234567",
                    enrollments: [{
                        courseId: "sampleId-1"
                    }]
                }); // Instantiate userModel
                expect(user.enrollments[0],"model does not have enrolledOn property in subdocument").to.have.property('enrolledOn');
            });

            it('should have a default enrolledOn date property within enrollments', () => {
                const user = new userModel({ // Instantiate userModel with default enrollments
                enrollments: [{
                    courseId: "sampleId-1"
                }]
                });
                expect(user.enrollments[0].enrolledOn,"enrolledOn property is not a date.").to.be.a('date')
            });

            // Test for status property within enrollments
            it('should have a status property', () => {
                const user = new userModel({
                    firstName: "John",
                    lastName: "Doe",
                    email: "jdoe2003@gmail.com",
                    password: "1234567",
                    enrollments: [{
                        courseId: "sampleId-1"
                    }]
                }); // Instantiate userModel
                expect(user.enrollments[0],"model does not have status property in subdocument").to.have.property('status');
            });

            it('should have a default status value of "Enrolled" of status property within enrollments', () => {
                const user = new userModel({ // Instantiate userModel with default enrollments
                enrollments: [{
                    courseId: "sampleId-1"
                }]
                
                });
                expect(user.enrollments[0].status.toLowerCase()).to.equal("enrolled");
            });

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

                const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

                
				global.gradesObject["s43"] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
                    feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s43 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {
	console.log(`S${sessionNumber} - Tested`);
}


