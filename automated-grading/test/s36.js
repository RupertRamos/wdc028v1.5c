//Sample
let sessionNumber = 36;

const { MongoClient } = require('mongodb');
const chai = require('chai');
const expect = chai.expect;
const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s36){

    try {

		// Requires the path to the index file inside the activity folder
		// const activity = require(`../backend/s${sessionNumber}/activity/index`);
		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

        const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);
        // console.log(activity)
        describe('s36', function() {
        let client;
        let db;
        let startTime;
        let errors = [];
        this.timeout(50000)
        // Increments the totalTests variable to use in computation of session score
        beforeEach(function () {
            totalTests += 1;
        })
        afterEach(function () {
            // this.currentTest.state allows to access the state of each test after it is run
            // returns either "passed" or "failed" for the "state" property
            if (this.currentTest.state == "passed") {
                passingTests += 1;
            }
            if (this.currentTest.state === 'failed') {
                errors.push({
                    test: this.currentTest.title,
                    feedback: this.currentTest.err.message
                });
            }
        })

        before(async () => {
            startTime = performance.now();
            // Connect to local MongoDB server
            client = await MongoClient.connect('mongodb+srv://admin:admin1234@cluster0.zlyew.mongodb.net/bookingAPI?retryWrites=true&w=majority', { useUnifiedTopology: true });
            db = client.db('inventory');
            db.fruits = db.collection('fruits')
            let res = await db.fruits.insertMany([
                {
                    name: "Apple",
                    color: "Red",
                    stock: 20,
                    price: 40,
                    supplier_id: 1,
                    onSale: true,
                    origin: ["Philippines", "US"],
                },
                {
                    name: "Banana",
                    color: "Yellow",
                    stock: 15,
                    price: 20,
                    supplier_id: 2,
                    onSale: true,
                    origin: ["Philippines", "Ecuador"],
                },
                {
                    name: "Kiwi",
                    color: "Green",
                    stock: 25,
                    price: 50,
                    supplier_id: 1,
                    onSale: true,
                    origin: ["US", "China"],
                },
                {
                    name: "Mango",
                    color: "Yellow",
                    stock: 10,
                    price: 120,
                    supplier_id: 2,
                    onSale: false,
                    origin: ["Philippines", "India"],
                }
            ]);

        });



        it('Use the count operator to count the total number of fruits on sale', async () => {
            // Retrieve a users document with the specified name
            
            let fruits = await activity.fruitsOnSale(db).then(result => (result.toArray()));

            fruits.forEach((fruit) => {
                expect(fruit.fruitsOnSale).to.equal(3,"Number of on sale fruits should only be 3");
            });
            
        });

        it('Use the count operator to count the total number of fruits with stock more than 20', async () => {
            // Retrieve a users document with the specified name
            
            let fruits = await activity.fruitsInStock(db).then(result => (result.toArray()));
            

            fruits.forEach((fruit) => {
                expect(fruit.enoughStock).to.equal(2,"Number of fruits with stock more than 20 should only be  2");
            });
            
        });


        it('Use the average operator to get the average price of fruits onSale per supplier', async () => {
            // Retrieve a users document with the specified name
            
            let fruits = await activity.fruitsAvePrice(db).then(result => (result.toArray()));
            
            fruits.forEach((fruit) => {

                if ((fruit._id === 1)) {
                    expect(fruit.avg_price).to.equal(45,"supplier id fruits on sale average price should be 45.");
                } 

                if ((fruit._id === 2)) {
                    expect(fruit.avg_price).to.equal(20,"supplier id fruits on sale average price should be 20.");
                } 

            });
            
        });


            it('Use the max operator to get the highest price of a fruit per supplier', async () => {
            // Retrieve a users document with the specified name
            
            let fruits = await activity.fruitsHighPrice(db).then(result => (result.toArray()));

            fruits.forEach((fruit) => {

                if ((fruit._id === 1)) {
                    expect(fruit.max_price).to.equal(50,"supplier id fruits on sale max price should be 50.");
                } 

                if ((fruit._id === 2)) {
                    expect(fruit.max_price).to.equal(20,"supplier id fruits on sale max price should be 20.");
                } 

            });
            
            });
        
        
                it('Use the min operator to get the lowest price of a fruit per supplier', async () => {
                // Retrieve a users document with the specified name
                
                let fruits = await activity.fruitsLowPrice(db).then(result => (result.toArray()));
                
                fruits.forEach((fruit) => {

                    if ((fruit._id === 1)) {
                        expect(fruit.min_price).to.equal(40,"supplier id fruits on sale min price should be 40.");
                    } 
    
                    if ((fruit._id === 2)) {
                        expect(fruit.min_price).to.equal(20,"supplier id fruits on sale min price should be 20.");
                    } 
    
                });
                
                });
        
                // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
                // Use of objects with the "session number" as keys to allow for ease of automation
                after(async function () {

                    const endTime = performance.now();
                    const totalTime = (endTime - startTime) / 1000; // Convert to seconds
                    console.log(`Total time taken: ${totalTime} seconds`);
                    
                    global.gradesObject[`s${sessionNumber}`] = {
                        grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
                        score: `${passingTests}/${totalTests}`,
                        feedback: errors.length ? errors : "No Errors"
                    }

                    //Drop Database after use
                    await db.dropDatabase()

                    // Disconnect from MongoDB server
                    await client.close();

                    jsonData.gradedTest.s36 = true;
                    fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
                });

        });

	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {

    console.log(`S${sessionNumber} - Tested`);

}


