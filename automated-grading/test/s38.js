// Variable to store session number for printing out the grade for the session
let sessionNumber = 38;

const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s38){

  try {

    // Requires the path to the index file inside the activity folder
    const app = require(`../sessions/backend/s38/activity/index`)

    // Variable to store passing unit test scores for the session
    let passingTests = 0;
    // Variable to store total number of session unit tests
    let totalTests = 0;

    describe(`s38`, function () {

      let startTime;
      let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
        if (this.currentTest.state === 'failed') {
          errors.push({
              test: this.currentTest.title,
              feedback: this.currentTest.err.message
          });
        }
			})
      // console.log("hello")
      it('should return "Welcome to Booking System" for "/" with GET request', (done)=>{
        chai.request(app)
        .get('/')
        .end((err, res) => {
           console.log(err)
          expect(res).to.have.status(200, 'Expecting status code should be 200');
          // console.log(res.text)

          expect(res.text.toLowerCase(),"response text does not include 'booking system'").to.include("welcome").include("booking").include("system")
          done();
        })
      })

      it('should return "Welcome to your profile" for "/profile" with GET request', (done)=>{
        chai.request(app)
        .get('/profile')
        .end((err, res) => {
          // expect(data.toLowerCase()).to.include("welcome").include("booking").include("system")
          expect(res).to.have.status(200, 'Expecting status code should be 200');
          // console.log(res.text)

          expect(res.text.toLowerCase(),"response text does not include 'profile'").to.include("profile")
          done();
        })
      })


      it(`should return "Here's our courses available" for "/courses" with GET request`, (done)=>{
        chai.request(app)
        .get('/courses')
        .end((err, res) => {
          // expect(data.toLowerCase()).to.include("welcome").include("booking").include("system")
          expect(res).to.have.status(200, 'Expecting status code should be 200');
          // console.log(res.text)

          expect(res.text.toLowerCase(),"response text does not include 'course'").to.include("course")
          done();
        })
      })


      it(`should return "Add a course to our resources" for "/addCourse" with POST request`, (done)=>{
        chai.request(app)
        .post('/addCourse')
        .end((err, res) => {
          // expect(data.toLowerCase()).to.include("welcome").include("booking").include("system")
          expect(res).to.have.status(200, 'Expecting status code should be 200');
          // console.log(res.text)

          expect(res.text.toLowerCase(),"response text does not include 'add' and 'course'").to.include("add").include("course")
          done();
        })
      })


      it(`should return "Update a course to our resources" for "/updateCourse" with PUT request`, (done)=>{
        chai.request(app)
        .put("/updateCourse")
        .end((err, res) => {
          // expect(data.toLowerCase()).to.include("welcome").include("booking").include("system")
          expect(res).to.have.status(200, 'Expecting status code should be 200');
          // console.log(res.text)

          expect(res.text.toLowerCase(),"response text does not include 'update' and 'course'").to.include("update").include("course")
          done();
        })
      })

      it(`should return "Archive courses to our resources" for "/archiveCourse" with DELETE request`, (done)=>{
        chai.request(app)
        .delete("/archiveCourse")
        .end((err, res) => {
          // expect(data.toLowerCase()).to.include("welcome").include("booking").include("system")
          expect(res).to.have.status(200, 'Expecting status code should be 200');
          // console.log(res.text)

          expect(res.text.toLowerCase(),"response text does not include 'archive' and 'course'").to.include("archive").include("course")
          done();
        })
      })

      // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
      // Use of objects with the "session number" as keys to allow for ease of automation
      after(function () {

        const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);
        
				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
          feedback: errors.length ? errors : "No Errors"
				}

        jsonData.gradedTest.s38 = true;
        fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
      });

    });
    
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
  console.log(`S${sessionNumber} - Tested`);
}



