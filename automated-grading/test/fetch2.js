let sessionNumber = 49;
require('geckodriver');
// Generated by Selenium IDE
const { Builder, By, Key, until } = require('selenium-webdriver')
const firefox = require('selenium-webdriver/firefox');
const { performance } = require('perf_hooks');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const chai = require('chai');
const expect = chai.expect;

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s49){

	try {

        if (!fs.existsSync('./sessions/fullstack/s49/discussion/index.html')) {
            throw new Error('File not found');
        }
		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;
			

		describe(`s${sessionNumber}`, function () {

			this.timeout(30000);

			let driver;
			let vars;
		
			let internalCssTestPassed = false;
		
			function parsePixels(value) {
				return parseFloat(value.replace('px', ''));
			}

			let startTime;

            


			before(async function() {

            const options = new firefox.Options();
            options.headless();

            options.setPreference('browser.download.manager.showWhenStarting', false);
            options.setPreference('browser.download.manager.useWindow', false);
            options.setPreference('devtools.netmonitor.enabled', true);

			  startTime = performance.now();
			  // Any setup code you need to run before the tests start
			  driver = await new Builder().forBrowser('firefox').setFirefoxOptions(options).build()

			});


			beforeEach(async function() {

			  totalTests += 1;
			  vars = {}
			})

			afterEach(function(){

				if(this.currentTest.state === "passed"){
					passingTests += 1;
				}

				process.removeAllListeners(); 

			})


            it('should delete and remove post', async function() {

                const filePath = path.join(__dirname, '..','fullstack','s49','discussion','index.html');
                const url = `file://${filePath}`;
                await driver.get(url);
                
                // Wait for the page to reload and the new post to be added
                await driver.wait(until.elementLocated(By.id('post-1')), 30000);
            
                // Check if the new post's title is displayed on the page
                const postDelete = await driver.findElement(By.id('post-1')).findElements(By.css("button"))
                await postDelete[1].click();

                // // Check if the div is no longer present on the page
                let elements = await driver.findElements(By.css("#post-1"));
                expect(elements.length).to.equal(0,"Added post is not deleted.")

              })

              it('should delete and remove post', async function() {


                const filePath = path.join(__dirname, '..','fullstack','s49','discussion','index.html');
                const url = `file://${filePath}`;
                await driver.get(url);
                
                // Wait for the page to reload and the new post to be added
                await driver.wait(until.elementLocated(By.id('post-2')), 30000);

                // execute the script after navigating to the target page
                const interceptFetchAndReturnMethod = async () => {
                  await driver.executeScript(() => {
                    // save the original fetch function
                    const originalFetch = window.fetch;
                    
                   
                    // create a new fetch function
                    window.fetch = function (url, options) {
                        // log the request method
                        window.reqM = options.method;
                    
                        // call the original fetch function
                        return originalFetch.apply(this, arguments);
                    }

                    });

                };
                
               await interceptFetchAndReturnMethod();
            
                // Check if the new post's title is displayed on the page
                const postDelete = await driver.findElement(By.id('post-2')).findElements(By.css("button"))
                await postDelete[1].click();

                let postMethod =  await driver.executeScript('return window.reqM')

                expect(postMethod).to.include("DELETE","Wrong request method was used.")


              })

              

            after(async function () {

   
                const endTime = performance.now();
                const totalTime = (endTime - startTime) / 1000; // Convert to seconds
                console.log(`Total time taken: ${totalTime} seconds`);
      
                global.gradesObject[`${sessionNumber}`] = {
                  grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
                  score: `${passingTests}/${totalTests}`
                }
            
                jsonData.gradedTest.s49 = true;
                //await fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
    
                // await driver.close()
                await driver.quit(); 
              });
		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND" || err.message === "File not found"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
	console.log(`S${sessionNumber} - Tested`);
}


