// Variable to store session number for printing out the grade for the session
let sessionNumber = 39;

const fetch = require("node-fetch");
globalThis.fetch = fetch

const assert = require('assert'); 
const expect = require("chai").expect;
const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s39){

    try {

        // Requires the path to the index file inside the activity folder
        const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);
        const requestCollection = fs.readFileSync(`./sessions/backend/s${sessionNumber}/activity/s39-activity.postman_collection.json`);
        const requests = JSON.parse(requestCollection);
        // Variable to store passing unit test scores for the session
        let passingTests = 0;
        // Variable to store total number of session unit tests
        let totalTests = 0;

        describe(`s${sessionNumber}`, function () {

            let startTime;
            let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
                if (this.currentTest.state === 'failed') {
                    errors.push({
                        test: this.currentTest.title,
                        feedback: this.currentTest.err.message
                    });
                }
			})


            it('getAllTodo() must be able to fetch all todo list items.', async () => {
                //{ userId: 1, id: 1, title: 'delectus aut autem', completed: false }

                let todoList = await activity.getAllToDo();
                expect(todoList.length).to.be.at.above(0,"retrieved todoList must have at least 1 document.");

            })

            it('getSpecificToDo() must fetch data with correct structure.', async () => {

                expect(await activity.getSpecificToDo(),"retrieved object is missing a property").to.have.all.keys("userId","id","title","completed");
                
            })

            it('createToDo() must create data and return with correct structure.', async () => {

                expect(await activity.createToDo(),"retrieved object is missing a property").to.have.all.keys("userId","id","title","completed");
                
            })

            it('updateToDo() must update data and return with correct structure.', async () => {

                expect(await activity.updateToDo(),"retrieved object is missing a property").to.have.all.keys("userId","id","title","description","status","dateCompleted");
                
            })

            it('deleteToDo() must delete data and return empty object as well as the correct method.', async () => {
                
                expect(await activity.deleteToDo(),"retrieved object must be empty").to.not.have.all.keys("userId","id","title","completed","dateCompleted");

            })

            it('postman request getAllToDo must have correct URL and http method.', async () => {

                let postmanReq = await requests.item.find(item => item.name === "getAllToDo")
                expect(postmanReq,"getAllToDo request does not exist.").to.exist;
                expect(postmanReq.request.url.raw).to.include("https://jsonplaceholder.typicode.com/todos","URL is incorrect");
                expect(postmanReq.request.method).to.equal("GET","HTTP Method is incorrect.");
                
            })

            it('postman request getSpecificToDo must have correct URL and http method.', async () => {

                let postmanReq = await requests.item.find(item => item.name === "getSpecificToDo")
                expect(postmanReq,"getSpecificToDo request does not exist.").to.exist;
                expect(postmanReq.request.url.raw).to.include("https://jsonplaceholder.typicode.com/todos/1","URL is incorrect");
                expect(postmanReq.request.method).to.equal("GET","HTTP Method is incorrect.");
                
            })

            it('postman request createToDo must have correct URL and http method.', async () => {

                let postmanReq = await requests.item.find(item => item.name === "createToDo")
                expect(postmanReq,"createToDo request does not exist.").to.exist;
                expect(postmanReq.request.url.raw).to.include("https://jsonplaceholder.typicode.com/todos","URL is incorrect");
                expect(postmanReq.request.method).to.equal("POST","HTTP Method is incorrect.");
                
            })

            it('postman request updateToDo must have correct URL and http method.', async () => {

                let postmanReq = await requests.item.find(item => item.name === "updateToDo")
                expect(postmanReq,"updateToDo request does not exist.").to.exist;
                expect(postmanReq.request.url.raw).to.include("https://jsonplaceholder.typicode.com/todos/1","URL is incorrect");
                expect(postmanReq.request.method).to.equal("PUT","HTTP Method is incorrect.");
                
            })

            it('postman request deleteToDo must have correct URL and http method.', async () => {

                let postmanReq = await requests.item.find(item => item.name === "deleteToDo")
                expect(postmanReq,"deleteToDo request does not exist.").to.exist;
                expect(postmanReq.request.url.raw).to.include("https://jsonplaceholder.typicode.com/todos/1","URL is incorrect");
                expect(postmanReq.request.method).to.equal("DELETE","HTTP Method is incorrect.");
                
            })



            // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
            // Use of objects with the "session number" as keys to allow for ease of automation
            after(function () {

                const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
                    feedback: errors.length ? errors : "No Errors"
				}
                jsonData.gradedTest.s39 = true;
                fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
            });

        });
        
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {
    console.log(`S${sessionNumber} - Tested`);
}


