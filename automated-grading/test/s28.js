// Variable to store session number for printing out the grade for the session
let sessionNumber = 28;

const assert = require('assert'); 
const expect = require("chai").expect;

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s28){

    try {

        // Requires the path to the index file inside the activity folder
        const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);

        // Variable to store passing unit test scores for the session
        let passingTests = 0;
        // Variable to store total number of session unit tests
        let totalTests = 0;

        describe(`s${sessionNumber}`, function () {

            let startTime;
            let errors = [];
			before(async function() {
			  startTime = performance.now();
              originalConsoleLog = console.log;
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

                if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})

            // Unit tests for the activity
            it('displayValues() should display cubed values in console', () => {

                const consoleOutput = [];
				console.log = (message) => consoleOutput.push(message);

                activity.displayValues([10,11,12]);
                console.log = originalConsoleLog;

                expect(consoleOutput).to.include(1000,"displayValues does not properly log the cubed value of a given array element.");
                expect(consoleOutput).to.include(1331,"displayValues does not properly log the cubed value of a given array element.");
                expect(consoleOutput).to.include(1728,"displayValues does not properly log the cubed value of a given array element.");

            });

            it('celsiusToFahrenheit() should returned mapped array of converted celsius degrees to fahrenheit', () => {

                let convertedVal = activity.celsiusToFahrenheit([37,75,100]);

                expect(convertedVal).to.include(98.6,"celsiusToFahrenheit does not properly return the converted value of a given array element.");
                expect(convertedVal).to.include(167,"celsiusToFahrenheit does not properly return the converted value of a given array element.");
                expect(convertedVal).to.include(212,"celsiusToFahrenheit does not properly return the converted value of a given array element.");


            });

            it('areAllEven() should return correct boolean if the given array are all even or not.', () => {

                expect(activity.areAllEven([6,8,10])).to.equal(true,"areAllEven does not properly return a true value when all numbers in given array are even.");
                expect(activity.areAllEven([2,7,10])).to.equal(false,"areAllEven does not properly return a false value when not all numbers in given array are even.");

            });

            it('hasDivisibleBy8() should return correct boolean if the given array has at least 1 number is divisible by 8 or otherwise.', () => {

                expect(activity.hasDivisibleBy8([1,3,16])).to.equal(true,"hasDivisibleBy8 does not properly return a true value when at least one number in the given array is divisible by 8.");
                expect(activity.hasDivisibleBy8([2,7,10])).to.equal(false,"areAllEven does not properly return a false value value when there is no number in the given array is divisible by 8");

            });

            it('filterEvenNumbers() returns a filtered array of even numbers.', () => {

                let filteredEven = activity.filterEvenNumbers([20,4,25,75,100]);

                expect(filteredEven).to.include(20,"filterEvenNumbers does not contain an expected even value");
                expect(filteredEven).to.include(4,"filterEvenNumbers does not contain an expected even value");
                expect(filteredEven).to.include(100,"filterEvenNumbers does not contain an expected even value");

            });

            it('getProductNames() returns an array of only product names', () => {

                let productsArray1 = [
                    {
                        name: "Shampoo",
                        price: 90
                    },
                    {
                        name: "Toothbrush",
                        price: 50
                    },
                    {
                        name: "Soap",
                        price: 25
                    },
                    {
                        name: "Toothpaste",
                        price: 45
                    },
                ];
                
                let productNames1 = activity.getProductNames(productsArray1)

                expect(productNames1).to.include("Shampoo","getProductNames returned array does not contain an expected value");
                expect(productNames1).to.include("Toothbrush","getProductNames returned array does not contain an expected value");

                let productsArray2 = [
                    {
                        name: "Gaming Mouse",
                        price: 2500
                    },
                    {
                        name: "Gaming Keyboard",
                        price: 6000
                    },
                ];
                
                let productNames2 = activity.getProductNames(productsArray2)

                expect(productNames2).to.include("Gaming Mouse","getProductNames returned array does not contain an expected value when passed a different array");
                expect(productNames2).to.include("Gaming Keyboard","getProductNames returned array does not contain an expected value when passed a different array");


            });


            // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
            // Use of objects with the "session number" as keys to allow for ease of automation
            after(async function () {

                console.log = originalConsoleLog;
                const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

                jsonData.gradedTest.s28 = true;
                await fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
            });

        });
        
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
    console.log(`S${sessionNumber} - Tested`);
}


