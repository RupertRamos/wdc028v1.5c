// Variable to store session number for printing out the grade for the session
let sessionNumber = 41;

const chai = require('chai');
// Chai http package allows for use of request method to process HTTP requests on API
const http = require('chai-http');
// Stores expect method from chai module to use for sending HTTP requests
const expect = chai.expect;
// Allows use of chai methods like "request", "post", etc.
chai.use(http);

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s41){

	try {
		

		// Requires the path to the index file inside the activity folder
		const app = require(`../sessions/backend/s${sessionNumber}/index`)

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		describe(`s${sessionNumber}`, function () {

			this.timeout(30000);

            let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
				if (this.currentTest.state === 'failed') {
                    errors.push({
                        test: this.currentTest.title,
                        feedback: this.currentTest.err.message
                    });
                }
			})

			// used to add a user to test for duplicate users
			it("test_api_register_user", (done)=>{
				chai.request(app)
				.post('/signup')
				.type('json')
				.send({
					username: "johndoe1",
					password: "johndoe1234"
				})
				.end((err, res) => {
					expect(res.status,"response status should be equal to 201").to.equal(201);
					done();
				})
			})

			// test for checking for duplicate users
			it("test_api_check_for_duplicates", (done)=>{

				chai.request(app)
				.post('/signup')
				.type('json')
				.send({
					username: "johndoe1",
					password: "johndoe1234"
				})
				.end((err, res) => {
					expect(res.text.toLowerCase(),"response text does not include 'duplicate'").to.include("duplicate");
					done();
				})
			})

			it("test_api_check_for_empty_inputs", (done)=>{

				chai.request(app)
				.post('/signup')
				.type('json')
				.send({
					username: "",
					password: ""
				})
				.end((err, res) => {
					expect(res.text.toLowerCase(),"response text is not equal to 'BOTH username and password must be provided.'").to.include("both").include("provided");
					done();
				})
			})

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function (){

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
                    feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s41 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
	console.log(`S${sessionNumber} - Tested`);
}



