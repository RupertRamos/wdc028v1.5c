# WDC028 - S34 - MongoDB - CRUD Operations

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/17bWumR-t9424AwXlJAApeKWhHUeFizg7qRdmnmvpw0w/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/17bWumR-t9424AwXlJAApeKWhHUeFizg7qRdmnmvpw0w/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s23) |
| Manual                  | Link                                                         |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                            | Link                                                         |
| ------------------------------------------------ | ------------------------------------------------------------ |
| Intro to MongoDB                                 | [Link](https://docs.mongodb.com/manual/introduction/)        |
| Databases and Collections                        | [Link](https://docs.mongodb.com/manual/core/databases-and-collections/) |
| Mongo DB Community Server Installation - Linux   | [Link](https://docs.mongodb.com/manual/administration/install-on-linux/) |
| Mongo DB Community Server Installation - MacOS   | [Link](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/) |
| Mongo DB Community Server Installation - Windows | [Link](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/) |
| Mongo DB Community Server                        | [Link](https://www.mongodb.com/try/download/community)       |
| Mongo DB Account Registration                    | [Link](https://account.mongodb.com/account/register)         |
| Google Public DNS                                | [Link](https://developers.google.com/speed/public-dns/docs/using) |
| Open DNS                                         | [Link](https://use.opendns.com/)                             |
| MongoDB Manage mongod process                    | [Link](https://docs.mongodb.com/manual/tutorial/manage-mongodb-processes/) |
| MongoDB Troubleshooting Connection Issues        | [Link](https://docs.atlas.mongodb.com/troubleshoot-connection/) |
| Robo3t Download                                  | [Link](https://studio3t.com/download/?source=robomongo&medium=homepage) |
| MongoDB Compass                                  | [Link](https://docs.mongodb.com/compass/current/install/)    |
| MongoDB insertOne Method                         | [Link](https://docs.mongodb.com/manual/reference/method/db.collection.insertOne/) |
| MongoDB insertMany Method                        | [Link](https://docs.mongodb.com/manual/reference/method/db.collection.insertMany/) |
| MongoDB find Method                              | [Link](https://docs.mongodb.com/manual/reference/method/db.collection.find/) |
| MongoDB pretty Method                            | [Link](https://docs.mongodb.com/manual/reference/method/cursor.pretty/) |
| MongoDB updateOne Method                         | [Link](https://docs.mongodb.com/manual/reference/method/db.collection.updateOne/) |
| MongoDB updateMany Method                        | [Link](https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/) |
| MongoDB replaceOne Method                        | [Link](https://docs.mongodb.com/manual/reference/method/db.collection.replaceOne/) |
| MongoDB deleteOne Method                         | [Link](https://docs.mongodb.com/manual/reference/method/db.collection.deleteOne/) |
| MongoDB deleteMany Method                        | [Link](https://docs.mongodb.com/manual/reference/method/db.collection.deleteMany/) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[10 mins] - What is MongoDB Atlas?
	2.[25 mins] - Code Discussion
		a. Registering an account in MongoDB Atlas
		b. Creation of Starter Cluster
	3.[25 mins] - Configuring the Connection to MongoDB Atlas
		a. Whitelisting of IP Address
		b. Getting the connection method
	4.[1 hr] - Connecting to MongoDB Atlas Database
		a. Setup in Robo3T
	5.[1 hr] - CRUD Operations
		a. Database Creation
		b. Create Records
		c. Read Records
		d. Update Records
		e. Delete Records
	6.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
