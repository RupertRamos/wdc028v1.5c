# Session Objectives

At the end of the session, the students are expected to:

- set up their MongoDB Atlas database in order to explore the CRUD operations.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s23)
- [Google Slide Presentation](https://docs.google.com/presentation/d/17bWumR-t9424AwXlJAApeKWhHUeFizg7qRdmnmvpw0w)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [MongoDB CRUD Operations (MongoDB Docs)](https://docs.mongodb.com/manual/crud/)

# Lesson Proper

## What is MongoDB Atlas?

MongoDB Atlas is a MongoDB database located in the cloud and can be deployed in a cloud platform like Amazon Web Services, Azure or Google Cloud Platform.

MongoDB Community Server can be installed on your local machine. However, since it is in your local machine, it is only accessible to the network you are currently in.

With MongoDB Atlas, we are assured that our data is already deployed in the cloud and less configuration will be needed later in the future sessions.

# Code Discussion

## Register an Account in MongoDB Atlas

Create an account in MongoDB Atlas using your Google Account.

Upon registering an account on MongoDB Atlas, the following pages might show up:

![readme-images/Untitled.png](readme-images/Untitled.png)

Select any of the free tiers available above. As a recommendation, choose the **Singapore** (ap-southeast-1) since it is the nearest location to Philippines.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

Change the name of the cluster to **Zuitt-Bootcamp** and click the **Create Cluster** button. After that, you will see this view:

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

After the cluster provisioning is finished, we can now access our database.

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

## Configuring the Connection to MongoDB Atlas

Click the **Connect** button on the left side of the view earlier. A popup like this should show:

![readme-images/Untitled%204.png](readme-images/Untitled%204.png)

For the first step, click the **Allow Access from Anywhere** option**.** It will show a field with IP address of **0.0.0.0/0**. Click the **Add IP Address** button.

For the second step, add a user with both username and password of **admin**.

![readme-images/Untitled%205.png](readme-images/Untitled%205.png)

## Connect to MongoDB Atlas Database

Click the **Choose a connection method** button from the previous step.

![readme-images/Untitled%206.png](readme-images/Untitled%206.png)

Select the **Connect using MongoDB Compass** option.

![readme-images/Untitled%207.png](readme-images/Untitled%207.png)

Copy the connection string from the second step.

Open the Robo3T in your local machine. Upon opening, there should be a popup like this:

![readme-images/Untitled%208.png](readme-images/Untitled%208.png)

Click the **Create** link. In the new popup, paste the copied connection string to the field beside the **From SRV** button, change the `**<password>**` in the connection string into the actual password of the user, then click the **From SRV** button.

![readme-images/Untitled%209.png](readme-images/Untitled%209.png)

Name the connection as **MongoDB Atlas** and click the **Test** button. The output of the test should be like this:

![readme-images/Untitled%2010.png](readme-images/Untitled%2010.png)

If the password has not been changed in the connection string and the **From SRV** and **Test** is clicked, the connection test. To make it successful, change the password, click **From SRV** again, then re-test the connection using the updated values.

After saving the connection settings, we should now see the following in the list of available connections:

![readme-images/Untitled%2011.png](readme-images/Untitled%2011.png)

Finally, click the **Connect** button with the selected entry to connect to the MongoDB Atlas database.

### Bonus: Why Robo3T and not MongoDB Compass?

The simple reason for this preference is that Robo3T consumes far less memory on your machine compared to MongoDB Compass. Below is an example in Windows:

![readme-images/Untitled%2012.png](readme-images/Untitled%2012.png)

MongoDB uses 11.495 times more memory than that of Robo3T, even without any heavy workload.

The reason why MongoDB Compass uses so much memory is because it is built using Electron.js — a JavaScript framework where developers can create desktop apps using web technologies. Due to this, apps built on Electron.js are using the same engine that powers Google Chrome (a.k.a. the RAM-eater). 

## CRUD Operations

CRUD stands for Create, Read, Update and Delete.

These operations allow us to manage our data.

Inside Robo3T, right-click on the MongoDB Atlas connection and select the **Create Database** option.

![readme-images/Untitled%2013.png](readme-images/Untitled%2013.png)

Create a database named **course_booking**.

![readme-images/Untitled%2014.png](readme-images/Untitled%2014.png)

Then, right-click on the database and click the Open Shell.

![readme-images/Untitled%2015.png](readme-images/Untitled%2015.png)

### Create

Inside the opened shell, write the following code:

```jsx
db.users.insertOne({
    "firstName": "John",
    "lastName": "Smith"
});
```

There should be a pane that will pop out and will contain an object that contains acknowledged and insertedId properties.

![readme-images/Untitled%2016.png](readme-images/Untitled%2016.png)

To insert multiple documents, we can use insertMany().

```jsx
db.users.insertMany([
    { "firstName": "John", "lastName": "Doe" },
    { "firstName": "Jane", "lastName": "Doe" }
]);
```

Instead of putting an object inside the parenthesis, we now place an array of objects.

![readme-images/Untitled%2017.png](readme-images/Untitled%2017.png)

### Read

To get all the inserted users, we can use find().

```jsx
db.users.find();
```

The pane below the shell should display the three documents we added earlier.

![readme-images/Untitled%2018.png](readme-images/Untitled%2018.png)

To find documents with a specified condition, an object should be passed to the find().

```jsx
db.users.find({ "lastName": "Doe" });
```

The find() should now display only "John Doe" and "Jane Doe".

![readme-images/Untitled%2019.png](readme-images/Untitled%2019.png)

### Update

Retrieve the ID of "John Smith" and update its data.

```jsx
db.users.updateOne(
    {
        "_id": ObjectId("ID_OF_JOHN_SMITH")
    },
    {
        $set: {
            "email": "johnsmith@gmail.com"
        }
    }
);
```

The first argument contains the identifier on which document to update and the second argument contains the properties to be updated.

Since email did not exist before the update, updateOne() will simply add the email property to the selected document.

![readme-images/Untitled%2020.png](readme-images/Untitled%2020.png)

To update multiple documents, we can use updateMany().

```jsx
db.users.updateMany(
    {
        "lastName": "Doe"
    },
    {
        $set: {
            "isAdmin": false
        }
    }
);
```

Now, all documents with last name of "Doe" has a property of isAdmin with a value of false.

![readme-images/Untitled%2021.png](readme-images/Untitled%2021.png)

### Delete

To delete multiple documents, we can use deleteMany().

```jsx
db.users.deleteMany({ "lastName": "Doe" });
```

Be careful when deleting data though! Make sure that when you delete, you use the most specific condition (e.g. using the ID of the document to be deleted).

Only show this to the students but do not execute this code in Robo3T.

We can use deleteOne instead when we're sure we want to delete only one document.

```jsx
db.users.deleteOne({ "_id": ObjectId("ID_OF_JOHN_SMITH") });
```

![readme-images/Untitled%2022.png](readme-images/Untitled%2022.png)

# Activity

## Instructions

Add the following users:

[Users](https://www.notion.so/a61c84db817a4e45ae509d7be5b21721)

Add the following courses:

[Courses](https://www.notion.so/4035363ccfd54f52be0cfb5ff58f3de9)

Get user IDs and add them as enrollees of the courses (update):

[Course Enrollees](https://www.notion.so/db4ffd9a77de4e22a8fa9e9d66e7a71b)

Get the users who are not administrators.

Save all the codes you used in a text file and push it to your GitLab repository.

## Expected Output

![readme-images/Untitled%2023.png](readme-images/Untitled%2023.png)

Find all users.

![readme-images/Untitled%2024.png](readme-images/Untitled%2024.png)

Find all courses.

![readme-images/Untitled%2025.png](readme-images/Untitled%2025.png)

Courses with enrollees.

![readme-images/Untitled%2026.png](readme-images/Untitled%2026.png)

Find users that are not administrators.

## Solution

```jsx
db.users.insertMany([
    { "firstName": "Diane", "lastName": "Murphy", "email": "dmurphy@mail.com", "isAdmin": false, "isActive": true },
    { "firstName": "Mary", "lastName": "Patterson", "email": "mpatterson@mail.com", "isAdmin": false, "isActive": true },
    { "firstName": "Jeff", "lastName": "Firrelli", "email": "jfirrelli@mail.com", "isAdmin": false, "isActive": true },
    { "firstName": "Gerard", "lastName": "Bondur", "email": "gbondur@mail.com", "isAdmin": false, "isActive": true },
    { "firstName": "Pamela", "lastName": "Castillo", "email": "pcastillo@mail.com", "isAdmin": true, "isActive": false },
    { "firstName": "George", "lastName": "Vanauf", "email": "gvanauf@mail.com", "isAdmin": true, "isActive": true }
]);

db.courses.insertMany([
    { "name": "Professional Development", "price": 10000 },
    { "name": "Business Processing", "price": 13000 }
]);

db.courses.updateOne(
    {
        "_id": ObjectId("5fbf59aa80562d8f7065c653")
    },
    {
        $set: {
            "enrollees": [
                { "userId": ObjectId("5fbf58d380562d8f7065c64d") },
                { "userId": ObjectId("5fbf58d380562d8f7065c650") }
            ]
        }
    }
);

db.courses.updateOne(
    {
        "_id": ObjectId("5fbf59aa80562d8f7065c654")
    },
    {
        $set: {
            "enrollees": [
                { "userId": ObjectId("5fbf58d380562d8f7065c64e") },
                { "userId": ObjectId("5fbf58d380562d8f7065c64f") }
            ]
        }
    }
);

db.users.find({ "isAdmin": false });
```