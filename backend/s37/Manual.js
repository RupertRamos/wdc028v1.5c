/*
==========================
S37 - Node.js Introduction
==========================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1H09Hsl0OVi74b3U-sYl9jvwWS2rrHSGbBrQmdH8EO_Q/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s26

Other References:
	Bootcamp Installation Guides
		https://drive.google.com/drive/u/0/folders/1lHKIYXYis1rolnyVTiLnQPPS7gglS65y
	What Does Module Mean
		https://www.techopedia.com/definition/3843/module
	An overview of HTTP
		https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview
	Node JS Documentation createServer
		https://nodejs.org/api/http.html#http_http_createserver_options_requestlistener
	Request Object
		https://developer.mozilla.org/en-US/docs/Web/API/Request
	Response Object
		https://developer.mozilla.org/en-US/docs/Web/API/Response
	What is a Computer Port
		https://www.cloudflare.com/learning/network-layer/what-is-a-computer-port/
	HTTP Response Status Codes
		https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
	Request Response Cycle
		https://iq.opengenus.org/content/images/2019/09/Untitled-1-.png
	Nodemon
		https://www.npmjs.com/package/nodemon
	npm install
		https://docs.npmjs.com/cli/v7/commands/npm-install
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
==========
*/

// 1. Verify installation of node JS
	// Terminal

		node --version

		/*
		Important Note:
			- If node JS is not installed the student's device, have them install it.
			- Refer to "references" section of this file to find the documentation for bootcamp installation guides.
		*/

// 2. Create a "discussion" folder and create an "index.js" file.
	// backend > s26 > discussion > index.js

// 3. Use the "require" directive to load the HTTP module of node JS.
	// Application > index.js

		// Use the "require" directive to load Node.js modules
		// A "module" is a software component or part of a program that contains one or more routines
		// The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol
		// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
		// HTTP is a protocol that allows the fetching of resources such as HTML documents
		// Clients (browser) and servers (node JS/express JS applications) communicate by exchanging individual messages.
		// The messages sent by the client, usually a Web browser, are called requests
		// The messages sent by the server as an answer are called responses.
		let http = require("http");

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for what a module is and an overview of the HTTP.
		*/

// 4. Create a server.
	// Application > index.js

		let http = require("http");

		// Using this module's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses back to the client
		// The http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
		// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
		http.createServer(function (request, response) {});

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for the Node JS documentation on the createServer method, request object and response object.
		*/

// 5. Define the port number that the server will be listening in to.
	// Application > index.js

		/*...*/

		// A port is a virtual point where network connections start and end.
		// Each port is associated with a specific process or service
		// The server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our server
		http.createServer(function (request, response) {}).listen(4000);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for what is a computer port.
		*/

// 6. Send a response back to the client.
	// Application > index.js

		/*...*/

		http.createServer(function (request, response) {

			// Use the writeHead() method to:
		    // Set a status code for the response - a 200 means OK
		    // Set the content-type of the response as a plain text message
			response.writeHead(200, {'Content-Type': 'text/plain'});
		   
			// Send the response with text content 'Hello World'
			response.end('Hello World');

		}).listen(4000);

		/*
		Important Note:
			- Refer to "references" section of this file to find a document for HTTP Response Status Codes and a diagram of the request response cycle.
		*/

// 7. Create a console log to indicate that the server is running.
	// Application > index.js

		/*...*/

		http.createServer(function (request, response) {

			/*...*/

		}).listen(4000);

		// When server is running, console will print the message:
		console.log('Server running at localhost:4000');

// 8. Run the server.
	// Application > Terminal

		node index.js

		/*
		Important Note:
			- Inputting the command tells our device to run node JS
			- index.js refers to the file that will be run
		*/

// 9. Access the port 4000 via the browser to see the response sent back to the client.
	// Browser > localhost:4000

// 10. Let's create another server. Create a "routes.js" file to create a different server.
	// Application > routes.js

		const http = require('http');

		// Creates a variable "port" to store the port number
		const port = 4000;

		// Creates a variable "app" that stores the output of the "createServer" method
		// This will allow us to use http createServer's other methods. 
		const app = http.createServer((request, response) => {});

		// Uses the "app" and "port" variables created above.
		// Since the app is now our defined server we can then use the listen method here to assign our port and run our server
		// This makes our code and server creating more readable rather than having to chain .listen() at the end of createServer() which is a little longer.
		app.listen(port);

		// When server is running, console will print the message:
		console.log(`Server now accessible at localhost:${port}.`);

		/*
			Important Notes:
				- Refactor the creation of the server by saving it in a variable for ease of use when listening to the app.
				- This also reflects server definition in ExpressJS.
		
		*/

// 11. Install nodemon.
	// Application > Terminal

		npm install -g nodemon

		/*
		Important Note:
			- Installing this package will allow the server to automatically restart when files have been changed for update
			- This will allow us to hot reload the application meaning any changes applied to the application will be applied without having to restart it
			- "npm install" is a command that allows us to install any package/depency that we would like to use with our applications
			- "-g" refers to a global installation where the current version of the package/dependency will be installed locally on our device allowing us to use nodemon on any project
			- Keep note that installing a package globally will always used the installed version and not the current version of the package
			- To update the locally installed package, reinstall it by triggering the same command above
			- Refer to "references" section of this file to find the documentation for nodemon and npm install.
		*/

// 12. Stop the previously running server and rerun the routes.js file using nodemon.
	// Application > Terminal

		// Stop the server
		ctrl + c

		// Run the routes.js file
		nodemon routes.js

		/*
		Important Note:
			- It's good practice to always stop any running process in our terminals before closing them.
			- Forcefully closing the terminal when a process is still running might lead to unexpected behavior.
			- A common error that students might encounter is the error that says "The specified port is already in use.".
			- Simply restarting the device is the best solution to this.
			- An alternative solution is to use the command "npx kill-port [port-number]" (e.g npx kill-port 4000).
			- Make sure that the correct port number is being killed as this might cause problems if an incorrect port number or a port number that was running a critical operating system process is suddenly killed.
		*/

// 13. Create a condition and a response when the route "/greeting" is accessed.
	// Application > routes.js

		/*...*/

		const app = http.createServer((request, response) => {

			// Accessing the "greeting" route returns a message of "Hello World"
			// "request" is an object that is sent via the client (browser)
			// The "url" property refers to the url or the link in the browser
		    if (request.url == '/greeting') {

		        response.writeHead(200, {'Content-Type': 'text/plain'})
		        response.end('Hello Again')

		    }

		});

		/*...*/

// 14. Access the "/greeting" route via the browser to see the response sent back to the client.
	// Browser > localhost:4000/greeting

// 15. Create a condition and a response when the route "/greeting" is accessed.
	// Application > routes.js

		/*...*/

		const app = http.createServer((request, response) => {

		    if (request.url == '/greeting') {

		        /*...*/

		    // Accessing the "homepage" route returns a message of "This is the homepage"
		    } else if (request.url == '/homepage') {

		        response.writeHead(200, {'Content-Type': 'text/plain'})
		        response.end('This is the homepage')

		    }

		});

		/*...*/

// 16. Access the "/homepage" route via the browser to see the response sent back to the client.
	// Browser > localhost:4000/homepage

// 17. Create a condition and a response when any other route is accessed.
	// Application > routes.js

		/*...*/

		const app = http.createServer((request, response) => {

		    if (request.url == '/greeting') {

		        /*...*/

		    } else if (request.url == '/homepage') {

		        /*...*/

		    // All other routes will return a message of "Page not available"
		    } else {

		    	// Set a status code for the response - a 404 means Not Found
		        response.writeHead(404, {'Content-Type': 'text/plain'})
		        response.end('Page not available')

		    }

		});

		/*...*/

// 18. Access any other route via the browser to see the response sent back to the client.
	// Browser > localhost:4000/hello

/*
========
Activity
========
*/

// Instructor Note: Copy the activity-template.js to your batch Boodle Notes.

/*

## Instructions that can be provided to the students for reference:

1. In the S31 folder, create an activity folder and an index.js file inside of it.
2. Copy the template code in boodle.
3. Create an index.js file inside of the activity folder.
	a. Paste the template code in your index.js
	b. Import the http module using the required directive.
4. Create a variable port and assign it with the value of 3000.
	a. Create a server using the createServer method that will listen in to the port provided above.
	b. Console log in the terminal a message when the server is successfully running.
5. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
	a. Access the login route to test if it’s working as intended.
6. Create a condition for any other routes that will return an error message.
	a. Access any other route to test if it’s working as intended.
7. Reminder: 
	a. Make sure to follow the server creation format from routes.js
8.	Update your local backend git repository and push to git with the commit message of Add activity code s37.
9.	Add the link in Boodle for s37.

*/



// 1. Create an "activity" folder and an "activity.js" file.
	// backend > S26 > activity > index.js

// 2. Use and Follow instructions in the Google Slides.
	// Copy the activity-template.js to your batch Boodle Notes.

// 3. Activity Solution
	// backend > S26 > activity > index.js

		// Use the "require" directive to load Node.js modules
		const http = require('http');
		// Creates a variable "port" to store the port number
		const port = 3000;

		// Creates a variable "server" that stores the output of the "createServer" method
		const app = http.createServer((request, response) => {

			// Accessing the "greeting" route returns a message of "Hello World"
			if (request.url == '/login') {

				response.writeHead(200, {'Content-Type': 'text/plain'})
				response.end('Welcome to the login page.')

			// All other routes will return a message of "Page not available"
			} else {

				// Set a status code for the response - a 404 means Not Found
				response.writeHead(404, {'Content-Type': 'text/plain'})
				response.end(`I'm sorry the page you are looking for cannot be found.`)

			}

		})

		// Uses the "server" and "port" variables created above.
		app.listen(port);

		// When server is running, console will print the message:
		console.log(`Server now accessible at localhost:${port}.`);

		module.exports =  {app}
