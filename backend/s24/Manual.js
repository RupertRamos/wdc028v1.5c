/*
================================================
S24 - JavaScript - Repetition Control Structures
================================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1ML4BRgRGPGYtfF8IGHRlFqjAMajQ9gDVdDXclzoZfdc/edit#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s16

Other References:
	JavaScript Expressions and Statement
		https://medium.com/launch-school/javascript-expressions-and-statements-4d32ac9c0e74
	Loops and Iteration
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration
	Statements
		https://www.teamten.com/lawrence/programming/intro/intro2.html
	Iteration
		https://teachcomputerscience.com/iterations/
	JavaScript While Loop
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/while
	Decrement Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Decrement
	JavaScript Do While Loop
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/do...while
	Number Function
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number
	JavaScript parseInt() Function
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt
	JavaScript for Loop
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for
	Increment Operator
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Increment
	JavaScript String
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
	JavaScript String .length Property
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/length
	JavaScript .toLowerCase() Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toLowerCase
	Continue Statement
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/continue
	Break Statement
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/break
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
1. Create an "index.html" file.
	Batch Folder > S16  > Discussion > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>JavaScript Loops</title>
		    </head>
		    <body>
		    </body>
		</html>
		*/

/*
2. Create an "index.js" file and to test if the script is properly linked to the HTML file.
	Application > index.js
*/

		console.log("Hello World!");

/*
3. Link the "index.js" script file to our HTML file.
	Application > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <!-- ... -->
		    <body>
		    	<script src="./index.js"></script>
		    </body>
		</html>
		*/

		/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
		*/

/*
4. Add code to the "index.js" file to demonstrate and discuss While Loops.
	Application > index.js
*/

		console.log("Hello World!");

		// [Section] While Loop
		/*
		    - A while loop takes in an expression/condition
		    - Expressions are any unit of code that can be evaluated to a value
		    - If the condition evaluates to true, the statements inside the code block will be executed
		    - A statement is a command that the programmer gives to the computer
		    - A loop will iterate a certain number of times until an expression/condition is met
		    - "Iteration" is the term given to the repetition of statements
		    - Syntax
		        while(expression/condition) {
		            statement
		        }
		*/

		
		let count = 5;

		// While the value of count is not equal to 0
		while(count !== 0) {

		    // The current value of count is printed out
		    console.log("While: " + count);

		    // Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
		    // Loops occupy a significant amount of memory space in our devices
		    // Make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop
		    // Forgetting to include this in loops will make our applications run an infinite loop which will eventually crash our devices
		    // After running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console quickly close the application/browser/tab to avoid this
		    count--;

		}
		

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for JavaScript Expressions and Statement, Loops and Iteration, Statements, Iteration, JavaScript While Loop and Decrement Operator.
		*/

/*
5. Add more code to demonstrate and discuss Do While Loops.
	Application > index.js
*/

		/*...*/

		while(count !== 0) {
			/*...*/
		}

		// [Section] Do While Loop
		/*
		    - A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.
		    - Syntax
		        do {
		            statement
		        } while (expression/condition)
		*/

		/*
		    - The "Number" function works similarly to the "parseInt" function
		    - Both differ significantly in terms of the processes they undertake in converting information into a number data type and other features that help with manipulating data
		    - The "prompt" function creates a pop-up message in the browser that can be used to gather user input
		    - How the Do While Loop works:
		        1. The statements in the "do" block executes once
		        2. The message "Do While: " + number will be printed out in the console
		        3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on given expression/condition (e.g. number less than 10)
		        4. If the expression/condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met
		        5. If the expression/condition is true, the loop will stop
		*/

		
		let number = Number(prompt("Give me a number"));

		do {

		    // The current value of number is printed out
		    console.log("Do While: " + number);

		    // Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
		    // number = number + 1
		    number += 1;

		// Providing a number of 10 or greater will run the code block once and will stop the loop
		} while (number < 10)
		
		/*
		Important Note:
			- In order to have the students focus on certain portions of the discussion, comment out previous loops defined in the "index.js" file.
			- This also prevents students with older model devices from crashing and slowing down during the discussion.
			- It is best to provide the students with the step by step procedure of the loops iteration to give them a better idea of how loops work.
			- Refer to "references" section of this file to find the documentations for JavaScript Do While Loop, Number Function and JavaScript parseInt() Function.
		*/

/*
6. Add more code to demonstrate and discuss For Loops.
	Application > index.js
*/

		/*...*/

		do {
		    /*...*/
		} while (number < 10)

		// [Section] For Loop
		/*
		    - A for loop is more flexible than while and do-while loops. It consists of three parts:
		        1. The "initialization" value that will track the progression of the loop.
		        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
		        3. The "finalExpression" indicates how to advance the loop.
		    - Syntax
		        for (initialization; expression/condition; finalExpression) {
		            statement
		        }
		*/

		/*
		    - Will create a loop that will start from 0 and end at 20
		    - Every iteration of the loop, the value of count will be checked if it is equal or less than 20
		    - If the value of count is less than or equal to 20 the statement inside of the loop will execute
		    - The value of count will be incremented by one for each iteration
		*/

		
		for (let count = 0; count <= 20; count++) {

		    // The current value of count is printed out
		    console.log(count);

		}
		
		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for JavaScript for Loop and Increment Operator.
		*/

/*
7. Create other loops to demonstrate For Loops.
	Application > index.js
*/

		for (let count = 0; count <= 20; count++) {
			/*...*/
		}

		
		let myString = "alex";
		// Characters in strings may be counted using the .length property
		// Strings are special compared to other data types in that it has access to functions and other pieces of information another primitive data type might not have
		console.log(myString.length);

		// Accessing elements of a string
		// Individual characters of a string may be accessed using it's index number
		// The first character in a string corresponds to the number 0, the next is 1 up to the nth number
		console.log(myString[0]);
		console.log(myString[1]);
		console.log(myString[2]);

		// Will create a loop that will print out the individual letters of the myString variable
		for(let x = 0; x < myString.length; x++){

		    // The current value of myString is printed out using it's index value
		    console.log(myString[x])

		}
		

		// Create a string named "myName" with a value of your name
		let myName = "AlEx";

		/*
		    - Creates a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel
		    - How this For Loop works:
		        1. The loop will start at 0 for the the value of "i"
		        2. It will check if "i" is less than the length of myName (e.g. 0)
		        3. The if statement will check if the value of myName[i] converted to a lowercase letter is equivalent to any of the vowels (e.g. myName[0] = a, myName[0] = e, myName[0] = i, myName[0] = o, myName[0] = u)
		        4. If the expression/condition is true the console will print the number 3.
		        5. If the letter is not a vowel the console will print the letter
		        6. The value of "i" will be incremented by 1 (e.g. i = 1)
		        7. Then the loop will repeat steps 2 to 6 until the expression/condition of the loop is false
		*/
		for (let i=0; i < myName.length; i++){

		    // console.log(myName[i].toLowerCase());

		    // If the character of your name is a vowel letter, instead of displaying the character, display number "3"
		    // The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
		    if (
		        myName[i].toLowerCase() == "a" ||
		        myName[i].toLowerCase() == "i" ||
		        myName[i].toLowerCase() == "u" ||
		        myName[i].toLowerCase() == "e" ||
		        myName[i].toLowerCase() == "o"
		    ){
		        // If the letter in the name is a vowel, it will print the number 3
		        console.log(3);
		    } else {
		        // Print in the console all non-vowel characters in the name
		        console.log(myName[i])
		    }

		}
		
		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for JavaScript String, JavaScript String .length Property and JavaScript .toLowerCase() Method.
		*/

/*
8. Add more code to demonstrate and discuss For Loops.
	Application > index.js
*/

		for (let i=0; i < myName.length; i++){
			/*...*/
		}

		// [Section] Continue and Break statements
		/*
		    - The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
		    - The "break" statement is used to terminate the current loop once a match has been found
		*/

		/*
			- Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
			- How this For Loop works:
			    1. The loop will start at 0 for the the value of "count".
			    2. It will check if "count" is less than the or equal to 20.
			    3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
			    4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
			    5. If the value of count is not equal to 0, the console will print the value of "count".
			    6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
			    7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
			    8. The value of "count" will be incremented by 1 (e.g. count = 1)
			    9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] > 10) is true, the loop will stop due to the "break" statement
		*/
		for (let count = 0; count <= 20; count++) {

		    // if remainder is equal to 0
		    if (count % 2 === 0) {

		        // Tells the code to continue to the next iteration of the loop
		        // This ignores all statements located after the continue statement;
		        continue;

		    }

		    // The current value of number is printed out if the remainder is not equal to 0
		    console.log("Continue and Break: " + count);

		    // If the current value of count is greater than 10
		    if (count > 10) {

		        // Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
		        // number values after 10 will no longer be printed
		        break;

		    }
		}
		

		let name = "alexandro";

		/*
		    - Creates a loop that will iterate based on the length of the string
		    - How this For Loop works:
		        1. The loop will start at 0 for the the value of "i"
		        2. It will check if "i" is less than the length of name (e.g. 0)
		        3. The if statement will check if the value of name[i] converted to a lowercase letter a (e.g. name[0] = a)
		        4. If the expression/condition of the if statement is true the loop will continue to the next iteration.
		        5. If the value of name[i] is not equal to a, the second if statement will be evaluated
		        6. The second if statement will check if the value of name[i] converted to a lowercase letter d (e.g. name[0] = d)
		        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
		        8. The value of "i" will be incremented by 1 (e.g. i = 1)
		        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] = d) is true, the loop will stop due to the "break" statement
		*/
		for (let i = 0; i < name.length; i++) {
		    
		    // The current letter is printed out based on it's index
		    console.log(name[i]);

		    // If the vowel is equal to a, continue to the next iteration of the loop
		    if (name[i].toLowerCase() === "a") {
		        console.log("Continue to the next iteration");
		        continue;
		    }

		    // If the current letter is equal to d, stop the loop
		    if (name[i] == "d") {
		        break;
		    }

		}

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for Continue Statement and Break Statement.
		*/

/*
========
Activity
========
*/

// Note: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.


/*
Instructions that can be provided to the students for reference:

Activity Reference:
JS Loops
https://www.w3schools.com/js/js_loop_for.asp
JS Break & Continue
https://www.w3schools.com/js/js_break.asp

Activity:

Member 1: 
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
	- Create an index.html file to attach our index.js file
	- Copy the template from boodle notes and paste it in an index.js file.
	- Update your local sessions git repository and push to git with the commit message of Add template code s24.
	- Console log the message Hello World to ensure that the script file is properly associated with the html file.
2. Create a JavaScript function called numberLooper that takes a number number as input.
	- The function should perform a loop that counts down from the given number to 0.
	- Add a variable called message. We will add messages to this variable.
3. In the loop, add the following conditions outlined below:
	- If the current value is less than or equal to 50, terminate the loop.
	- Update the message variable with the following message:
	- “The current value is at <count>. Terminating the loop.”
Member 2:
4. If the current value is divisible by 10:
	- Log this message in the console:
	- “The number is divisible by 10. Skipping the number.”
	- Then, Add a way to continue next iteration.
5. If the current value is divisible by 5:
	- Log the current value of the counter variable in the console.
6. After the loop finishes, return the message variable.

Member 3:
7. Create a for Loop that will iterate through the individual letters of the given string variable based on it’s length.

Member 4:
8. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

Member 5:
9 .Create an else statement that will add the current letter being looped to the given filteredString variable.

All Members:
10. Check out to your own git branch with git checkout -b <branchName>
11. Update your local sessions git repository and push to git with the commit message of Add activity code s24.
12. Add the sessions repo link in Boodle for s24.


*/
/* Solution */

// console.log("Hello World");
function numberLooper(number){
			
	//Create a message variable that will contain the messages after the loop has finished.
	let message;

	// Creates a loop that will use the number provided by the user and count down to 0
	for (let count = number; count >= 0; count--) {

		// Check what is the starting value of the loop
		console.log(count);

		// If the value provided is less than or equal to 50, terminate the loop
		if (count <= 50) {

			message = "The current value is at " + count + ". Terminating the loop.";
			console.log(message);

			break;

		// If the value is divisible by 10, skip printing the number
		} else if (count % 10 === 0) {

			console.log("The number is divisible by 10. Skipping the number.");
			continue;

		// If the value is divisible by 5, print the number
		} else if (count % 5 === 0) { 

			console.log(count);

		}

	}

	return message;

}



let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

// Creates a loop that will iterate through the whole string
for (let i=0; i < string.length; i++) {

	// Check what is the starting value of the loop
	// console.log(string[i]);
	
	// If the current letter being evaluated is a vowel
	if (
		string[i].toLowerCase() == 'a' ||
		string[i].toLowerCase() == 'e' ||
		string[i].toLowerCase() == 'i' ||
		string[i].toLowerCase() == 'o' ||
		string[i].toLowerCase() == 'u'
	) {

		// Continue the loop to the next letter/character in the sequence
		continue;

	// If the current letter being evaluated is not a vowel
	} else {

		// Add the letter to a different variable
		filteredString += string[i];

	}

}

// After the loop is complete, print the filtered string without the vowels
console.log(filteredString);


//Do not modify
//For exporting to test.js
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch(err){

}