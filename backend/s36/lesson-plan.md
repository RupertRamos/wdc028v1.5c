# WDC028 - S30 - MongoDB - Aggregation and Query Case Studies

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/11TTrf0pxffQWwNX2MkYh7BovKz-pdQSWeUU8MEODYRs/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/11TTrf0pxffQWwNX2MkYh7BovKz-pdQSWeUU8MEODYRs/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s25) |
| Manual                  | Link                                                         |

[Back to top](#table-of-contents)

## Reference Materials

| Topic               | Link                                                         |
| ------------------- | ------------------------------------------------------------ |
| MongoDB aggregation | [Link](https://docs.mongodb.com/manual/aggregation/)         |
| MongoDB $match      | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/match/) |
| MongoDB $group      | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/group/) |
| MongoDB $sum        | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/sum/) |
| MongoDB Data Models | [Link](https://docs.mongodb.com/manual/core/data-model-design/) |
| MongoDB $project    | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/project/) |
| MongoDB $sort       | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/sort/) |
| MongoDB $unwind     | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/unwind/) |
| MongoDB $count      | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/count/) |
| MongoDB $avg        | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/avg/) |
| MongoDB $min        | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/min/) |
| MongoDB $max        | [Link](https://docs.mongodb.com/manual/reference/operator/aggregation/max/) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[10 mins] - Aggregation in MongoDB
	2.[10 mins] - Use Cases for Aggregations
	3.[10 mins] - Aggregation Pipelines
	4.[10 mins] - Aggregation Pipeline Syntax
	5.[40 mins] - Counting Documents
	6.[20 mins] - Single-Purpose Aggregation Operations
	7.[40 mins] - Pipelines with Multiple Stages
	8.[40 mins] - Guidelines on Schema Design
		- One-To-One Relationship
		- One-To-Few Relationship
		- One-To-Many Relationship
	9.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
