# Session Objectives

At the end of the session, the students are expected to:

- learn how to make complex queries by using the aggregation functions of MongoDB.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s25)
- [Google Slide Presentation](https://docs.google.com/presentation/d/11TTrf0pxffQWwNX2MkYh7BovKz-pdQSWeUU8MEODYRs)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [Aggregation (MongoDB Docs)](https://docs.mongodb.com/manual/aggregation/)

# Lesson Proper

## Aggregation in MongoDB

An **aggregate** is a cluster of things that have come or have been brought together.

In MongoDB, **aggregation** operations **group** values from **multiple** documents together.

Aggregations are **needed** when your application **needs a form** of information that is **not visibly** available from your documents.

## Use Cases for Aggregations

Here are some example scenarios where you would want to use aggregations in MongoDB:

- You would like to know which students have not finished their courses yet.
- Count the total number of courses a student has completed.
- Determine the number of students who completed a specified course.

If you find yourself having these types of use cases, aggregations can come in handy.

# Code Discussion

## Aggregation Pipelines

The **aggregation pipeline** in MongoDB is a **framework** for data aggregation.

The MongoDB aggregation pipeline consists of **stages**.

Each stage **transforms** the documents as they pass through the pipeline.

Pipeline stages **do not need** to produce one output document for every input document; e.g. some stages may generate **new** documents or **filter** out documents.

![readme-images/Untitled.png](readme-images/Untitled.png)

Visualization of an aggregation operation from MongoDB.

## Aggregation Pipeline Syntax

The basic syntax for writing the aggregation pipeline looks like this:

```jsx
db.collection.aggregate({
  {stageA},
  {stageB},
  {stageC}
});
```

A pipeline can have one or more stages and each stage contains specific instructions in aggregating the documents.

## Counting Documents

To continue with the discussion, let's use the following code and add them to **aggregate_db** in our MongoDB Atlas.

```jsx
db.course_bookings.insertMany([
  { "courseId": "C001", "studentId": "S004", "isCompleted": true },
  { "courseId": "C002", "studentId": "S001", "isCompleted": false },
  { "courseId": "C001", "studentId": "S003", "isCompleted": true },
  { "courseId": "C003", "studentId": "S002", "isCompleted": false },
  { "courseId": "C001", "studentId": "S002", "isCompleted": true },
  { "courseId": "C004", "studentId": "S003", "isCompleted": false },
  { "courseId": "C002", "studentId": "S004", "isCompleted": true },
  { "courseId": "C003", "studentId": "S007", "isCompleted": false },
  { "courseId": "C001", "studentId": "S005", "isCompleted": true },
  { "courseId": "C004", "studentId": "S008", "isCompleted": false },
  { "courseId": "C001", "studentId": "S013", "isCompleted": true }
]);
```

Let's start with a simple aggregation operation. Specifically, let's try counting all the documents in the collection.

To do the counting, we have the following code:

```jsx
db.course_bookings.aggregate([
  { $group: { _id: null, count: { $sum: 1 } } },
]);
```

We only have one stage in this aggregation pipeline. It uses what is called the **group operator** symbolized by **$group** and groups documents into a single document.

In this case, the grouping is based on "_id = null" which results in all documents being combined into one and finally creating a **count** field which derives its value from the **$sum** operator.

The $sum operator has a value of **1**. When this stage is executed, it will give that value to every single document in the group then evaluates its sum. Therefore, if we have three documents, the count field will have a value of 1 each and then summed into a single value (1+1+1=3).

This can be translated to: "Group all course bookings regardless of their IDs and count each document in the group."

## Single-Purpose Aggregation Operations

If we only need simple aggregations (no more than one stage), we can alternatively use what is called **single purpose aggregation operations**.

```jsx
db.course_bookings.count();
```

While this method is definitely shorter than the one presented earlier, this will no longer let you have the flexibility of having more than one pipeline stage offered by the **aggregate()** method.

## Pipelines with Multiple Stages

Now let's try aggregating documents using two pipeline stages.

```jsx
db.course_bookings.aggregate([
  { $match: { isCompleted: true } },
  { $group: { _id: "$courseId", total: { $sum: 1 } } }
]);
```

The first stage retrieves documents wherein the students have completed the course.

The second stage then groups the retrieved documents from the first stage according to their courseId as the _id of the new set of documents, then counting how many documents are there in each group.

# Activity

## Instructions

Develop the code for the following instructions:

- Count the completed courses of student S013.
- Show students who are not yet completed in their course, without showing the course IDs.
- Sort the courseId in descending order while studentId in ascending order.

Each bullet must have their own aggregation pipeline code. Save the code in a file (**activity.js**) and push it to your GitLab repository.

## Expected Output

Count the completed courses of student S013.

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

Show students who are not yet completed in their course, without showing the course IDs.

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

Sort the courseId in descending order while studentId in ascending order.

![readme-images/Untitled%203.png](readme-images/Untitled%203.png)

## Solution

```jsx
db.course_bookings.aggregate([
  { $match: { isCompleted: true, studentId: "S013" } },
  { $group: { _id: null, count: { $sum: 1 } } }
]);

db.course_bookings.aggregate([
  { $match: { isCompleted: false } },
  { $project: { courseId: 0 } }
]);

db.course_bookings.aggregate([
  { $sort: { courseId: -1, studentId: 1 } }
]);
```