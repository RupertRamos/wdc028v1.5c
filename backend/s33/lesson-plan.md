# WDC028 - S33 - MongoDB - Data Modeling and Translation

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1tr9pSk7H7An1O5JNq1jXBgVB0QFEfxOpydxbfIJy9cA/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1tr9pSk7H7An1O5JNq1jXBgVB0QFEfxOpydxbfIJy9cA/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s22) |
| Manual                  | Link                                                         |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                               | Link                                                         |
| ----------------------------------- | ------------------------------------------------------------ |
| Data Model Design (MongoDB Docs)    | [Link](https://docs.mongodb.com/manual/core/data-model-design/) |
| Diagrams.net                        | [Link](Diagrams.net)                                         |
| Entity Relationship Diagram Symbols | [Link](https://www.lucidchart.com/pages/ER-diagram-symbols-and-meaning) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[20 mins] - What is a data model?
	2.[20 mins] - Data modelling case simulation
	3.[20 mins] - Creating the data models
	4.[1 hr] - Identifying relationships between models
	5.[1 hr] - Translating data models into an ERD
		- What is an Entity-Relationship Diagram?
		- Drawing an ERD using diagrams.net
		- Converting the ERD into a JSON-like syntax
	6.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
