/*
===========================================
S43 - Express.js - API Development (Part 1)
===========================================
Booking System API - Business Use Case Translation to Model Design
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1zwiGuycV29zRoRuVNjg31baNcsxDhhYiXU0CRBrdkqc/edit#slide=id.gc44b0f8b88_0_669
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s32-s36

Other References:
	npm Documentation
		https://docs.npmjs.com/
	npm init
		https://docs.npmjs.com/cli/v7/commands/npm-init
	Express JS Documentation - Getting Started
		https://expressjs.com/en/starter/installing.html
	Express JS express Function
		https://expressjs.com/en/api.html#express
	Express JS json Method
		https://expressjs.com/en/api.html#express.json
	Express JS urlencoded Method
		https://expressjs.com/en/api.html#express.urlencoded
	Mongoose Documentation
		https://mongoosejs.com/docs/api.html
	Mongoose connect Method
		https://mongoosejs.com/docs/connections.html
	Cross-Origin Resource Sharing (CORS)
		https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
	npm CORS package
		https://www.npmjs.com/package/cors
	Mongoose Schemas
		https://mongoosejs.com/docs/guide.html
	JavaScript Date object
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
	MVC Framework
		https://www.tutorialspoint.com/mvc_framework/mvc_framework_introduction.htm
	
	Creating Git Projects:
			GitLab
				https://gitlab.com/projects/new#blank_project
			GitHub
				https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
==========
Discussion
========== 
*/

/*
1. Create an "s43-s47" folder and create an empty "index.js" file that will serve as the entry point for our application.
	Application > index.js
*/

		/*
		Important Note:
			- Creating this file first is important before triggering the command to create a Node JS application in the following step.
			- Doing this first will automatically set the index.js file in the "package.json" file that will be created when the Node JS application is created.
		*/

/*
2. Create a Node JS application.
	Application > Terminal
*/

		npm init --y

		/*
		Important Note:
			- Triggering the command above will create a "package.json" file that will contain the details about our Node JS application.
			- If the "index.js" file was not created beforehand, you may change the "main" property to "index.js" to ensure that it would be the correct file that will be used as the entry point for the Node JS application.
			- Upon creation of this file, make sure to check that the correct file is set to ensure no problems will be encountered when trying to run the application.
			- Refer to "references" section of this file to find the documentations for npm Documentation and npm init.
		*/

/*
3. Install the Express JS Framework and cors package to be used in creating a basic Express JS application.
	Application > Terminal
*/

		npm install express

		/*
		Important Note:
			- Triggering the command above will install the latest version of Express JS framework and the cors package that we'll be using to easily setup a backend application.
			- There may be instances when an older version of a package is needed for a project, one reason for this is that a latest version of an application may have code breaking bugs or a preferred version of a package would like to be used.
			- To install older versions of packages, the following syntax may be used:
				- npm install packageName@versionNumber
			- The "cors" package will allow our backend application to be available to our frontend application.
			- By default our backend's CORS setting will prevent any application outside of our Express JS app to process requests to it. Using the cors package will allow us to manipulate this and control what applications may use our app.
			- Refer to "references" section of this file to find the documentation for Express JS Documentation - Getting Started.
		*/

/*
4. Create a simple Express JS application.
	Application > index.js
*/
	// [SECTION]Dependencies and Modules
		const express = require("express");
		
	// [SECTION]Environment Setup
		const port = 4000;

	// [SECTION]Server Setup
		// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
		const app = express();

		app.use(express.json());
		app.use(express.urlencoded({extended:true}));

		// Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
		// This syntax will allow flexibility when using the application locally or as a hosted application
		

	// [SECTION]Server Gateway Response
		//if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly.
		//else, if it is needed to be imported, it will not run the app and instead export it to be used in another file.
		if(require.main === module){
			app.listen(process.env.PORT || port, () => {
			    console.log(`API is now online on port ${ process.env.PORT || port }`)
			});
		}

		module.exports = {app,mongoose};
		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for Express JS express Function, Express JS json Method and Express JS urlencoded Method.
		*/

/*
5. Run the Express JS app.
	Application > Terminal
*/

		nodemon index.js

/*
6. Install the mongoose package.
	Application > Terminal
*/

		npm install mongoose

		/*
		Important Note:
			- Mongoose is a package that we will be using to connect and communicate with our MongoDB Atlas as well as gain access to methods that will make CRUD operations easier to do.
			- Refer to "references" section of this file to find the documentation for Mongoose Documentation.
		*/

/*
7. Connect our Express JS application to our MongoDB Atlas database.
	Application > index.js
*/

		const express = require("express");
		const mongoose = require("mongoose");
		
		const app = express();

		app.use(express.json());
		/*...*/

	//[SECTION] Database Connection 
		// Connect to our MongoDB database
		mongoose.connect("mongodb+srv://admin:admin123@cluster0.7iowx.mongodb.net/S32-S36?retryWrites=true&w=majority", {
				useNewUrlParser: true,
				useUnifiedTopology: true
			});
		// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
		mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

		

		/*
		Important Note:
			- Mongoose is a package that we will be using to connect and communicate with our MongoDB Atlas as well as gain access to methods that will make CRUD operations easier to do.
			- Refer to "references" section of this file to find the documentation for Mongoose connect Method.
		*/

/*
8. Install the cors package.
	Application > Terminal
*/

		npm install cors

		/*
		Important Note
			- By default our backend's CORS setting will prevent any application outside of our Express JS app to process requests to it. Using the cors package will allow us to manipulate this and control what applications may use our app.
			- Refer to "references" section of this file to find the documentations for Cross-Origin Resource Sharing (CORS) and npm CORS package.
		*/

/*
8. Implement cors in our backend application.
	Application > Terminal
*/

		/*...*/
		const mongoose = require("mongoose");
		// Allows our backend application to be available to our frontend application
		// Allows us to control the app's Cross Origin Resource Sharing settings
		const cors = require("cors");

	// Server Setup
		app.use(express.urlencoded({ extended: true }));
		// Allows all resources to access our backend application
		app.use(cors());

		/*...*/
		mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));
	

		/*
		Important Note:
			- Using the above syntax to implement CORS in our application is not best practice because it allows all other applications to access our backend app.
			- This is only used during development for ease of access.
			- The proper way of doing this will be taught during the CSP3 hosting session.
		*/

/*
9. Create a "model" folder and create a "Course.js" file to store the schema for our courses.
	Application > models > Course.js
*/
	//[SECTION] Dependencies and Modules	
		const mongoose = require("mongoose");

	//[SECTION] Schema/Blueprint
		const courseSchema = new mongoose.Schema({
			name : {
				type : String,
				// Requires the data for this our fields/properties to be included when creating a record.
				// The "true" value defines if the field is required or not and the second element in the array is the message that will be printed out in our terminal when the data is not present
				required : [true, "Course is required"]
			},
			description : {
				type : String,
				required : [true, "Description is required"]
			},
			price : {
				type : Number,
				required : [true, "Price is required"]
			},
			isActive : {
				type : Boolean,
				default : true
			},
			createdOn : {
				type : Date,
				// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a course is created in our database
				default : new Date()
			},
			// The "enrollees" property/field will be an array of objects containing the user IDs and the date and time that the user enrolled to the course
			// We will be applying the concept of referencing data to establish a relationship between our courses and users 
			enrollees : [
				{
					userId : {
						type : String,
						required: [true, "UserId is required"]
					},
					enrolledOn : {
						type : Date,
						default : new Date() 
					}
				}
			]
		})

	//[SECTION] Model
		module.exports = mongoose.model("Course", courseSchema);

		/*
		Important Note
			- Make sure to follow the naming convention for model files which should have the folowing:
				- The first letter of the file should be capitalized to indicate that we are accessing the model file.
				- Should use the singular form of the collection.
			- We will be following the Model-View-Controllers Framework in creating our backend and frontend application.
			- This allows us to follow a certain standard when developing our applications and separating files into their own folders will allow us to easily locate the necessary files we need in developing our app.
			- Refer to "references" section of this file to find the documentations for Mongoose Schemas, JavaScript Date object and MVC Framework.
		*/

/*
========
Activity
========
*/

/*
Activity References:

Mongoose JS Models
https://mongoosejs.com/docs/models.html

Mongoose Subdocuments
https://mongoosejs.com/docs/subdocs.html

Activity

Member 1:
1. Update your local sessions folder with the git commit message "Added discussion code s43"

Member 1,2,3,4,5:
2. Create a User model with the following properties:
- firstName - String
- lastName - String
- email - String
- password - String
- isAdmin - String
- mobileNo - String
- enrollments - Array of objects
	- courseId - String
	- enrolledOn - Date (Default value - new Date object)
	- status - String (Default value - Enrolled)

Member 5:
3. Make sure that all the fields are required to ensure consistency in the user information being stored in our database.

All Members:
4. Check out to your own git branch with git checkout -b <branchName>
5. Update your local sessions git repository and push to git with the commit message of Add activity code s43.
6. Add the sessions repo link in Boodle for s43.


*/


/*
Solution:

1. Create a "User.js" file to store the schema for our users.
	Application > models > User.js
*/

	//[SECTION] Modules and Dependencies
		const mongoose = require("mongoose");

	//[SECTION] Schema/Blueprint
		const userSchema = new mongoose.Schema({
			firstName : {
				type : String,
				required : [true, "First name is required"]
			},
			lastName : {
				type : String,
				required : [true, "Last name is required"]
			},
			email : {
				type : String,
				required : [true, "Email is required"]
			},
			password : {
				type : String,
				required : [true, "Password is required"]
			},
			isAdmin : {
				type : Boolean,
				default : false
			},
			mobileNo : {
				type : String, 
				required : [true, "Mobile No is required"]
			},
			// The "enrollments" property/field will be an array of objects containing the course IDs, the date and time that the user enrolled to the course and the status that indicates if the user is currently enrolled to a course
			enrollments : [
				{
					courseId : {
						type : String,
						required : [true, "Course ID is required"]
					},
					enrolledOn : {
						type : Date,
						default : new Date()
					},
					status : {
						type : String,
						default : "Enrolled"
					}
				}
			]
		})

	//[SECTION] Model
		module.exports = mongoose.model("User", userSchema);
