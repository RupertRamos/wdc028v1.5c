/*
===============================================
S21 - JavaScript - Basic Functions
===============================================
*/

/*
Other References:
	JavaScript Functions
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions
	JavaScript Function Parameters
		https://blog.alexdevero.com/function-parameters-arguments/#function-parameters
	JavaScript Function Arguments
		https://blog.alexdevero.com/function-parameters-arguments/#function-arguments
	JavaScript return Statement
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/return
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application
*/

/*
==========
Discussion
========== 
*/

/*
1. Create an "index.html" file.
    Batch Folder > S17  > Discussion > index.html
*/

        /*
        <!DOCTYPE HTML>
        <html>
            <head>
                <title>JavaScript - Basic Functions</title>
            </head>
            <body>
            </body>
        </html>
        */

/*
2. Create an "index.js" file and to test if the script is properly linked to the HTML file.
    Application > index.js
*/

        console.log("Hello World!");


/*
3. Link the "index.js" script file to our HTML file.
    Application > index.html
*/

        /*
        <!DOCTYPE HTML>
        <html>
            <!-- ... -->
            <body>
                <script src="./index.js"></script>
            </body>
        </html>
        */
        /*

/*
4. Add code to the "index.js" file to demonstrate and discuss Functions.
    Application > index.js
*/

//[ SECTION ] Functions
	// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function


//Function declarations
	//(function statement) defines a function with the specified parameters.

	/*
	Syntax:
		function functionName() {
			code block (statement)
		}
	*/
	// function keyword - used to defined a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.


		function printName(){
		    console.log("My name is John");
		};
	

		printName();

		//Semicolons are used to separate executable JavaScript statements.

//[ SECTION ] Function Invocation
	//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	//It is common to use the term "call a function" instead of "invoke a function".

		//Let's invoke/call the functions that we declared.
		printName();

		//declaredFunction(); - results in an error, much like variables, we cannot invoke a function we have yet to define. 

//[ SECTION ] Function declarations vs expressions
	//Function Declarations

		//A function can be created through function declaration by using the function keyword and adding a function name.

		//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
		
		//declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

		//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

		function declaredFunction() {

			console.log("Hello World from declaredFunction()")

		}

		declaredFunction();

	//Function Expression
		//A function can also be stored in a variable. This is called a function expression.

		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function - a function without a name.

		//variableFunction();
		/* 
			error - function expressions, being stored 
			in a let or const variable, cannot be hoisted.
		*/

		let variableFunction = function() {

			console.log("Hello Again!");

		};

		variableFunction();

		//A function expression of a function named func_name assigned to the variable funcExpression
		//How do we invoke the function expression?
		//They are always invoked (called) using the variable name.


		let funcExpression = function funcName() {

			console.log("Hello From The Other Side")


		}

		funcExpression();

	//You can reassign declared functions and function expressions to new anonymous functions.


		declaredFunction = function(){
			console.log("updated declaredFunction")
		}

		declaredFunction();


		funcExpression = function (){
			console.log("updated funcExpression");
		}

		funcExpression();

	//However, we cannot re-assign a function expression initialized with const.

		const constantFunc = function(){
			console.log("Initialized with const!")
		}

		constantFunc();

/*		
		constantFunc = function(){
			console.log("Cannot be re-assigned!")
		}

		constantFunc(); 

*/

//[ SECTION ] Function scoping

/*	
	Scope is the accessibility (visibility) of variables.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function
*/	

		{
			let localVar = "Armando Perez";
		}

		let globalVar = "Mr. Worldwide";

		console.log(globalVar);
		//console.log(localVar);//result in error.

		function showNames() {

			//Function scoped variables:
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Jane"; 

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);

		}

		showNames();

		//console.log(functionVar);//results to an error.
		//console.log(functionConst);//results to an error.
		//console.log(functionLet);//results to an error.

		/*
			The variables, functionVar,functionConst and functionLet, are function scoped and cannot be accessed outside of the function they were declared in.
		*/

	//Nested Functions

		//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

		function myNewFunction(){
			let name ="Jane";

			function nestedFunction(){
				let nestedName = "John"
				console.log(name);
			}

			//console.log(nestedName);//results to an error.
			//nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in.
			nestedFunction();
		}

		myNewFunction();
		//nestedFunction();//results to an error. 

		// Moreover, since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in.

	//Function and Global Scoped Variables

		//Global Scoped Variable
		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz"

			//Variables declared Globally (outside any function) have Global scope.
			//Global variables can be accessed from anywhere in a Javascript 
			//program including from inside a function.
			console.log(globalName);
		}
		
		myNewFunction2();

		//console.log(nameInside);//results to an error. 
			//nameInside is function scoped. It cannot be accessed globally.



//[ SECTION ] Using alert()

	//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

	alert("Hello World!");//This will run immediately when the page loads.

	//You can use an alert() to show a message to the user from a later function invocation.
	function showSampleAlert(){
		alert("Hello, User!");
	}

	showSampleAlert();

	//You will find that the page waits for the user to dismiss the dialog before proceeding. You can witness this by reloading the page while the console is open.

	console.log("I will only log in the console when the alert is dismissed.");

	//Notes on the use of alert():
		//Show only an alert() for short dialogs/messages to the user. 
		//Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

//[ SECTION ] Using prompt()
	
	//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

	let samplePrompt = prompt("Enter your Name.");

	//console.log(typeof samplePrompt);//The value of the user input from a prompt is returned as a string.
	console.log("Hello, " + samplePrompt);

	/*
		Syntax:

		prompt("<dialogInString>");


	*/

	let sampleNullPrompt = prompt("Don't enter anything.");

	console.log(sampleNullPrompt);//prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

	//prompt() can be used to gather user input and be used in our code. However, since prompt() windows will have the page wait until the user dismisses the window it must not be overused.

	//prompt() used globally will be run immediately, so, for better user experience, it is much better to use them accordingly or add them in a function.

	//Let's create a function scoped variables to store the returned data from our prompt(). This way, we can dictate when to use a prompt() window or have a reusable function to use our prompts.
	function printWelcomeMessage(){
		let firstName = prompt("Enter Your First Name");
		let lastName = prompt("Enter Your Last Name");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
	}

	printWelcomeMessage();



//[ SECTION ] Function Naming Conventions
	//Function names should be definitive of the task it will perform. It usually contains a verb.

		function getCourses(){

			let courses = ["Science 101","Math 101","English 101"];
			console.log(courses); 

		};

		getCourses();

	//Avoid generic names to avoid confusion within your code.

		function get(){

			let name = "Jamie";
			console.log(name);

		};

		get();

	//Avoid pointless and inappropriate function names.

		function foo(){

			console.log(25%5);

		};

		foo();

	//Name your functions in small caps. Follow camelCase when naming variables and functions.

		function displayCarInfo(){

			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");

		}
		
		displayCarInfo();


/*
========
Activity
========
*/

// Note: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.

/*
## Instructions that can be provided to the students for reference:

1. In the S17 folder, create an activity folder, an index.html file inside of it and link the index.js file.

2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.

3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.

4. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.


5. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.


6. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.


7. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.

8. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			You can also invoke the function in the console to view the returned value of the function.

			Note: This is optional.

9. Update your local backend git repository and push to git with the commit message of Add activity code s17.
10. Add the link in Boodle for s17.

*/



/*

## Solution:

	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.

*/

function getUserInfo(){

	return {

		name: "John Doe",
		age: 25,
		address: "123 Street, Quezon City",
		isMarried: false,
		petName: "Danny"

	}
}

let userInfo = getUserInfo();
console.log(userInfo);

/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.
	
*/

function getArtistsArray(){
	return ["Ben & Ben","Arthur Nery","Linkin Park","Paramore","Taylor Swift"];
}

let bandsArray = getArtistsArray();
console.log(bandsArray);


/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.
*/

function getSongsArray(){
	return ["Kathang Isip","Binhi","In the End","Bring by Boring Brick","Love Story"];
}

let songsArray = getSongsArray();
console.log(songsArray);


/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.
*/

function getMoviesArray(){
	return ["The Lion King","Meet the Robinsons","Howl's Moving Castle","Tangled","Frozen"];
}

let moviesArray = getMoviesArray();
console.log(moviesArray);


/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			You can also invoke the function in the console to view the returned value of the function.

			Note: This is optional.
*/


function getPrimeNumberArray(){
	return [2,3,5,7,17]
};


let primeNumberArray = getPrimeNumberArray();
console.log(primeNumberArray)



//Do not modify
//For exporting to test.js
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){

}











