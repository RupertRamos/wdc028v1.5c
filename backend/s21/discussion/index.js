//console.log("Hello World!");
//Functions
	// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function


// Function declarations
	//(function statement) defines a function with the specified parameters.

	/*
	Syntax:
		function functionName() {
			code block (statement)
		}
	*/
	// function keyword - used to defined a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.


		function printName(){
		    console.log("My name is John");
		};
	

		printName();

		//Semicolons are used to separate executable JavaScript statements.

//Function Invocation
	//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	//It is common to use the term "call a function" instead of "invoke a function".

		//Let's invoke/call the functions that we declared.
		printName();

		//declaredFunction(); - results in an error, much like variables, we cannot invoke a function we have yet to define. 

// Function declarations vs expressions
	//Function Declarations

		//A function can be created through function declaration by using the function keyword and adding a function name.

		//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
		
		declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

		//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

		function declaredFunction() {

			console.log("Hello World from declaredFunction()")

		}

		declaredFunction();

	//Function Expression
		//A function can also be stored in a variable. This is called a function expression.

		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function - a function without a name.

		//variableFunction();
		/* 
			error - function expressions, being stored 
			in a let or const variable, cannot be hoisted.
		*/

		let variableFunction = function() {

			console.log("Hello Again!");

		};

		variableFunction();

		// We can also create a function expression of a named function.
		// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
		// Function Expressions are always invoked (called) using the variable name.


		let funcExpression = function funcName() {

			console.log("Hello From The Other Side");


		}

		funcExpression();

	//You can reassign declared functions and function expressions to new anonymous functions.


		declaredFunction = function(){
			console.log("updated declaredFunction")
		}

		declaredFunction();


		funcExpression = function (){
			console.log("updated funcExpression");
		}

		funcExpression();

	//However, we cannot re-assign a function expression initialized with const.

		const constantFunc = function(){
			console.log("Initialized with const!")
		}

		constantFunc();

/*		
		constantFunc = function(){
			console.log("Cannot be re-assigned!")
		};//results in an error

		constantFunc(); 

*/

// Function scoping

/*	
	Scope is the accessibility (visibility) of variables within our program.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/	

		{
			let localVar = "Armando Perez";
		}

		let globalVar = "Mr. Worldwide";

		console.log(globalVar);
		//console.log(localVar);//console.log(localVar);//result in error. localVar, being in a block, cannot be accessed outside of its code block.

	//Function Scope
/*		
		JavaScript has function scope: Each function creates a new scope.
		Variables defined inside a function are not accessible (visible) from outside the function.
		Variables declared with var, let and const are quite similar when declared inside a function.
*/

		function showNames() {

			//Function scoped variables:
			var functionVar = "Joe";
			const functionConst = "John";
			let functionLet = "Jane"; 

			console.log(functionVar);
			console.log(functionConst);
			console.log(functionLet);

		}

		showNames();

		//console.log(functionVar);//results to an error.
		//console.log(functionConst);//results to an error.
		//console.log(functionLet);//results to an error.

		/*
			The variables, functionVar,functionConst and functionLet, are function scoped and cannot be accessed outside of the function they were declared in.
		*/

	//Nested Functions

		//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

		function myNewFunction(){
			let name ="Jane";

			function nestedFunction(){
				let nestedName = "John"
				console.log(name);
			}

			//console.log(nestedName);//results to an error.
			//nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in.
			nestedFunction();
		}

		myNewFunction();
		//nestedFunction();//results to an error. 

		// Moreover, since this function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in.

	//Function and Global Scoped Variables

		//Global Scoped Variable
		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz"

			//Variables declared Globally (outside any function) have Global scope.
			//Global variables can be accessed from anywhere in a Javascript 
			//program including from inside a function.
			console.log(globalName);
		};
		
		myNewFunction2();

		//console.log(nameInside);//results to an error. 
			//nameInside is function scoped. It cannot be accessed globally.

//The Return statement

/*
	The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.
	returnUserName() is a function that is able to return a value which can then be saved in a variable.

*/

	function returnFullName() {

	    return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
	    console.log("This message will not be printed.");
	}

/*

	In our example, the "returnFullName" function was invoked/called in the same line as declaring a variable.
	Whatever value is returned from the "returnFullName" function can be stored in a variable.

*/
	let fullname = returnFullName();
	console.log(fullname);


/*	
	This way, a function is able to return a value we can further use/manipulate in our program instead of only printing/displaying it in the console.
	Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.
	In this example, console.log() will print the returned value of the returnFullName() function.
*/

	console.log(returnFullName());

/*	

	You can also create a variable inside the function to contain the result and return that variable instead. You can do this for ease of use or for readability.

*/
	function returnFullAddress(){

		let fullAddress = {

			street: "#44 Maharlika St.",
			city: "Cainta",
			province: "Rizal",
		
		};

		return fullAddress;

	}

	let myAddress = returnAddress();
	console.log(myAddress);


//On the other hand, when a function the only has console.log() to display its result it will return undefined instead:

	function printPlayerInfo(){

		console.log("Username: " + "white_knight");
		console.log("Level: " + 95);
		console.log("Job: " + "Paladin");

	};

	let user1 = printPlayerInfo();
	console.log(user1);


	// returns undefined because printPlayerInfo returns nothing. It only displays the details in the console. 
	// You cannot save any value from printPlayerInfo() because it does not return anything.

	// You can return almost any data types from a function.

	function returnSumOf5and10(){
		return 5 + 10
	}

	let sumOf5And10 = returnSumOf5and10();
	console.log(sumOf5And10);

	let total = 100 + returnSumOf5and10();
	console.log(total);


	//Simulates getting an array of user names from a database
	function getGuildMembers(){

		return ["white_knight","healer2000","masterThief100","andrewthehero","ladylady99"]

	}

	console.log(getGuildMembers());


//Function Naming Conventions
	//Function names should be definitive of the task it will perform. It usually contains a verb.

	function getCourses(){

		let courses = ["Science 101","Math 101","English 101"];

		return courses;

	};

	let courses = getCourses();
	console.log(courses);

	//Avoid generic names to avoid confusion within your code.

	function get(){

		let name = "Jamie";
		return name;

	};

	let name = get();
	console.log(name);

	//Avoid pointless and inappropriate function names.

	function foo(){

		return 25%5;

	};

	console.log(foo());


	//Name your functions in small caps. Follow camelCase when naming variables and functions.

		function displayCarInfo(){

			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");

		}
		
		displayCarInfo();
