# WDC028 - S29 - MongoDB - Query Operators and Field Projection

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1QWixmhFZkbEm_NCwipxPIsJpz1RUkAx5bXK8HLRJBno/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1QWixmhFZkbEm_NCwipxPIsJpz1RUkAx5bXK8HLRJBno/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s24) |
| Manual                  | Link                                                         |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                            | Link                                                         |
| ------------------------------------------------ | ------------------------------------------------------------ |
| Importing and Exporting Databases from RoboMongo | [Link](https://medium.com/@salonimalhotra1ind/how-to-import-export-database-from-robomongo-ed9cec35efe) |
| mongoexport CLI command                          | [Link](https://docs.mongodb.com/database-tools/mongoexport/) |
| mongoimport CLI command                          | [Link](https://docs.mongodb.com/database-tools/mongoimport/) |
| MongoDB Greater Than Operator                    | [Link](https://docs.mongodb.com/manual/reference/operator/query/gt/) |
| MongoDB Greater Than Or Equal To Operator        | [Link](https://docs.mongodb.com/manual/reference/operator/query/gte/) |
| MongoDB Less Than Operator                       | [Link](https://docs.mongodb.com/manual/reference/operator/query/lt/) |
| MongoDB Less Than Or Equal To Operator           | [Link](https://docs.mongodb.com/manual/reference/operator/query/lte/) |
| MongoDB Not Equal Operator                       | [Link](https://docs.mongodb.com/manual/reference/operator/query/ne/) |
| MongoDB In Operator                              | [Link](https://docs.mongodb.com/manual/reference/operator/query/in/) |
| MongoDB Or Operator                              | [Link](https://docs.mongodb.com/manual/reference/operator/query/or/) |
| MongoDB And Operator                             | [Link](https://docs.mongodb.com/manual/reference/operator/query/and/) |
| MongoDB Slice Operator                           | [Link](https://docs.mongodb.com/manual/reference/operator/projection/slice/) |
| MongoDB Regex Operator                           | [Link](https://docs.mongodb.com/manual/reference/operator/query/regex/) |
| MongoDB Field Projection                         | [Link](https://docs.mongodb.com/manual/tutorial/project-fields-from-query-results/) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[1 hr] - Query Operators
		- $gt
		- $gte
		- $lt
		- $lte
		- $ne
		- $in
		- $or
		- $and
		- $slice
	2.[30 mins] - Field Projection
		- Inclusion
		- Exclusion
	3.[1hr 30 mins] - Evaluation Query Operators
	4.[1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)
