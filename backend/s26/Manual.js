/*
=====================================
S26 - JavaScript - Array Traversal
=====================================
*/

/*

Reference Material:
	Discussion Slides
		https://docs.google.com/presentation/d/1n6Cl7jYEM93uxqRxnKWh_FboFfiBQdKPR3tCQc8SwO8/edit?pli=1#slide=id.g53aad6d9a4_0_728
	Gitlab Repo
		https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s17

Other References:
	JavaScript Arrays
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
		https://www.w3schools.com/js/js_arrays.asp
	/Method_definitions
	Why Do Arrays Start With Index 0
		https://albertkoz.com/why-does-array-start-with-index-0-65ffc07cbce8
	JavaScript Methods
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions
	Javascript Array Methods
		https://dev.to/ibrahima92/javascript-array-methods-mutator-vs-non-mutator-15e2
	Mutable Definition
		https://developer.mozilla.org/en-US/docs/Glossary/Mutable
	JavaScript Array push Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push
	JavaScript Array pop Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/pop
	JavaScript Array unshift Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift
	JavaScript Array shift Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/shift
	JavaScript Array splice Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice
	JavaScript Array sort Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
	JavaScript Array reverse Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse
	Non-Mutator Array Methods In JavaScript
		http://www.discoversdk.com/blog/non-mutator-array-methods-in-javascript
	JavaScript Array indexOf Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
	JavaScript Array lastIndexOf Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf
	JavaScript Array slice Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice
	JavaScript Array toString Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/toString
	JavaScript Array concat Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat
	JavaScript Array join Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join
	JavaScript Array forEach Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
	JavaScript Array map Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
	JavaScript Array every Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every
	JavaScript Array some Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some
	JavaScript Array filter Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
	JavaScript String includes Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
	JavaScript Reduce Method
		https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
	Creating Git Projects:
		GitLab
			https://gitlab.com/projects/new#blank_project
		GitHub
			https://github.com/new
	Boodle
		https://boodle.zuitt.co/login/

Definition of terms:
	Batch Folder - The batch folder that will store all files of the students throughout the bootcamp. Normally located in the "Documents" folder of the device.
	Application - Root folder for the application

*/

/*
1. Create an "index.html" file.
	Batch Folder > S17  > Discussion > index.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <head>
		        <title>JavaScript Arrays</title>
		    </head>
		    <body>
		    </body>
		</html>
		*/

/*
2. Create an "script.js" file and to test if the script is properly linked to the HTML file.
	Application > script.js
*/

		console.log("Hello World!");

/*
3. Link the "script.js" file to our HTML file.
	Application > script.html
*/

		/*
		<!DOCTYPE HTML>
		<html>
		    <!-- ... -->
		    <body>
		    	<script src="./script.js"></script>
		    </body>
		</html>
		*/

		/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
		*/

/*
4. Add code to the "script.js" file to demonstrate and discuss Arrays and Indexes.
	Application > script.js
*/

		console.log("Hello World!");

		// [SECTION] Arrays and Indexes
		/*
		    - Arrays are used to store multiple related values in a single variable
		    - They are declared using square brackets ([]) also known as "Array Literals"
		    - Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
		    - Arrays also provide access to a number of functions/methods that help in achieving this
		    - A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
		    - Majority of methods are used to manipulate information stored within the same object
		    - Arrays are also objects which is another data type
		    - The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
		    - Syntax
		        let/const arrayName = [elementA, elementB, ElementC...]
		*/

		// Common examples of arrays
		let grades = [98.5, 94.3, 89.2, 90.1];
		let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
		// Possible use of an array but is not recommended
		let mixedArr = [12, 'Asus', null, undefined, {}]; 

		// Alternative way to write arrays
		let myTasks = [
		    'drink html',
		    'eat javascript',
		    'inhale css',
		    'bake sass'
		];

		// Reassigning array values
		console.log('Array before reassignment');
		console.log(myTasks);
		myTasks[0] = 'hello world';
		console.log('Array after reassignment');
		console.log(myTasks);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for JavaScript Arrays.
		*/

/*
5. Add more code to demonstrate and discuss Reading from Arrays.
	Application > script.js
*/

		/*...*/
		console.log(myTasks);

		// [SECTION] Reading from Arrays
		/*
		    - Accessing array elements is one of the more common tasks that we do with an array
		    - This can be done through the use of array indexes
		    - Each element in an array is associated with it's own index/number
		    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
		    - The reason an array starts with 0 is due to how the language is designed
		    - Array indexes actually refer to an address/location in the device's memory and how the information is stored
		    - Example array location in memory
		        Array address: 0x7ffe9472bad0
		        Array[0] = 0x7ffe9472bad0
		        Array[1] = 0x7ffe9472bad4
		        Array[2] = 0x7ffe9472bad8
		    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
		    - Syntax
		        arrayName[index];
		*/
		console.log(grades[0]);
		console.log(computerBrands[3]);
		// Accessing an array element that does not exist will return "undefined"
		console.log(grades[20]);

		// Getting the length of an array
		/*
		    - Arrays have access to the ".length" property similar to strings to get the number of elements present in an array
		    - This is useful for executing code that depends on the contents of an array
		*/
		console.log(computerBrands.length);

		if(computerBrands.length > 5){
		    console.log('We have too many suppliers. Please coordinate with the operations manager.');
		};

		// Accessing the last element of an array
		// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element
		let lastElementIndex = computerBrands.length - 1;

		console.log(computerBrands[lastElementIndex]);

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentation for Why Do Arrays Start With Index 0.
		*/

/*
6. Add more code to demonstrate and discuss Array Methods and Array Mutator Methods.
	Application > script.js
*/

		/*...*/

		console.log(computerBrands[lastElementIndex]);

		// [SECTION] Array Methods

		// Mutator Methods
		/*
		    - Mutator methods are functions that "mutate" or change an array after they're created
		    - These methods manipulate the original array performing various tasks such as adding and removing elements
		*/

		let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

		// push()
		/*
		    - Adds an element in the end of an array AND returns the array's length
		    - Syntax
		        arrayName.push();
		*/
		console.log('Current array:');
		console.log(fruits);
		let fruitsLength = fruits.push('Mango');
		console.log(fruitsLength);
		console.log('Mutated array from push method:');
		console.log(fruits);

		// Adding multiple elements to an array
		fruits.push('Avocado', 'Guava');
		console.log('Mutated array from push method:');
		console.log(fruits);

		// pop()
		/*
		    - Removes the last element in an array AND returns the removed element
		    - Syntax
		        arrayName.pop();
		*/
		let removedFruit = fruits.pop();
		console.log(removedFruit);
		console.log('Mutated array from pop method:');
		console.log(fruits);

		// unshift()
		/*
		    - Adds one or more elements at the beginning of an array
		    - Syntax
		        arrayName.unshift('elementA');
		        arrayName.unshift('elementA', elementB);
		*/
		fruits.unshift('Lime', 'Banana');
		console.log('Mutated array from unshift method:');
		console.log(fruits);

		// shift()
		/*
		    - Removes an element at the beginning of an array AND returns the removed element
		    - Syntax
		        arrayName.shift();
		*/
		let anotherFruit = fruits.shift();
		console.log(anotherFruit);
		console.log('Mutated array from shift method:');
		console.log(fruits);

		// splice()
		/* 
		    - Simultaneously removes elements from a specified index number and adds elements
		    - Syntax
		        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
		*/
		fruits.splice(1, 2, 'Lime', 'Cherry');
		console.log('Mutated array from splice method:');
		console.log(fruits);

		// sort()
		/*
		    - Rearranges the array elements in alphanumeric order
		    - Syntax
		        arrayName.sort();
		*/
		fruits.sort();
		console.log('Mutated array from sort method:');
		console.log(fruits);

		// reverse()
		/*
		    - Reverses the order of array elements
		    - Syntax
		        arrayName.reverse();
		*/
		fruits.reverse();
		console.log('Mutated array from reverse method:');
		console.log(fruits);

		/*
		Important Note:
			- At this point, the students are still not familiar with the concept of methods.
			- Give the students a brief explanation of what JavaScript methods are and inform them that this will be discussed in the next session.
			- The "sort" method is used for more complicated sorting functions.
			- Focus the students on the basic usage of the sort method and discuss it's more advanced usage only when you are confident in the topic.
			- Discussing this might confuse the students more given the amount of topics found in the session.
			- Refer to "references" section of this file to find the documentations for JavaScript Methods, Javascript Array Methods, Mutable Definition, JavaScript Array push Method, JavaScript Array pop Method, JavaScript Array unshift Method, JavaScript Array shift Method, JavaScript Array splice Method, JavaScript Array sort Method and JavaScript Array reverse Method.
		*/

/*
7. Add more code to demonstrate and discuss Array Non-Mutator Methods.
	Application > script.js
*/	
	
		/*...*/
		console.log(fruits);

		// Non-Mutator Methods
		/*
		    - Non-Mutator methods are functions that do not modify or change an array after they're created
		    - These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
		*/

		let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

		// indexOf()
		/*
		    - Returns the index number of the first matching element found in an array
		    - If no match was found, the result will be -1.
		    - The search process will be done from first element proceeding to the last element
		    - Syntax
		        arrayName.indexOf(searchValue);
		        arrayName.indexOf(searchValue, fromIndex);
		*/
		let firstIndex = countries.indexOf('PH');
		console.log('Result of indexOf method: ' + firstIndex);

		let invalidCountry = countries.indexOf('BR');
		console.log('Result of indexOf method: ' + invalidCountry);

		// lastIndexOf()
		/*
		    - Returns the index number of the last matching element found in an array
		    - The search process will be done from last element proceeding to the first element
		    - Syntax
		        arrayName.lastIndexOf(searchValue);
		        arrayName.lastIndexOf(searchValue, fromIndex);
		*/
		// Getting the index number starting from the last element
		let lastIndex = countries.lastIndexOf('PH');
		console.log('Result of lastIndexOf method: ' + lastIndex);

		// Getting the index number starting from a specified index
		let lastIndexStart = countries.lastIndexOf('PH', 6);
		console.log('Result of lastIndexOf method: ' + lastIndexStart);

		// slice()
		/*
		    - Portions/slices elements from an array AND returns a new array
		    - Syntax
		        arrayName.slice(startingIndex);
		        arrayName.slice(startingIndex, endingIndex);
		*/

		// Slicing off elements from a specified index to the last element
		let slicedArrayA = countries.slice(2);
		console.log('Result from slice method:');
		console.log(slicedArrayA);

		// Slicing off elements from a specified index to another index
		let slicedArrayB = countries.slice(2, 4);
		console.log('Result from slice method:');
		console.log(slicedArrayB);

		// Slicing off elements starting from the last element of an array
		let slicedArrayC = countries.slice(-3);
		console.log('Result from slice method:');
		console.log(slicedArrayC);

		// toString()
		/*
		    - Returns an array as a string separated by commas
		    - Syntax
		        arrayName.toString();
		*/
		let stringArray = countries.toString();
		console.log('Result from toString method:');
		console.log(stringArray);

		// concat()
		/*
		    - Combines two arrays and returns the combined result
		    - Syntax
		        arrayA.concat(arrayB);
		        arrayA.concat(elementA);
		*/
		let tasksArrayA = ['drink html', 'eat javascript'];
		let tasksArrayB = ['inhale css', 'breathe sass'];
		let tasksArrayC = ['get git', 'be node'];

		let tasks = tasksArrayA.concat(tasksArrayB);
		console.log('Result from concat method:');
		console.log(tasks);

		// Combining multiple arrays
		console.log('Result from concat method:');
		let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
		console.log(allTasks);

		// Combining arrays with elements
		let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
		console.log('Result from concat method:');
		console.log(combinedTasks);

		// join()
		/*
		    - Returns an array as a string separated by specified separator string
		    - Syntax
		        arrayName.join('separatorString');
		*/

		let users = ['John', 'Jane', 'Joe', 'Robert'];

		console.log(users.join());
		console.log(users.join(''));
		console.log(users.join(' - '));

		/*
		Important Note:
			- Refer to "references" section of this file to find the documentations for Non-Mutator Array Methods In JavaScript, JavaScript Array indexOf Method, JavaScript Array lastIndexOf Method, JavaScript Array slice Method, JavaScript Array toString Method, JavaScript Array concat Method and JavaScript Array join Method.
		*/

/*
8. Add more code to demonstrate and discuss Array Iteration Methods.
	Application > script.js
*/

		/*...*/
		console.log(users.join(' - '));

		// Iteration Methods
		/*
		    - Iteration methods are loops designed to perform repetitive tasks on arrays
		    - Useful for manipulating array data resulting in complex tasks
		*/

		// foreach()
		/*
		    - Similar to a for loop that iterates on each array element
		    - Variable names for arrays are normally written in the plural form of the data stored in an array
		    - It's common practice to use the singular form of the array content for parameter names used in array loops
		    - Array iteration methods normally work with a function supplied as an argument
		    - How these function works is by performing tasks that are pre-defined within an array's method
		    - Syntax
		        arrayName.forEach(function(indivElement) {
		            statement
		        })
		*/
		allTasks.forEach(function(task) {
		    console.log(task);
		});

		// Using forEach with conditional statements
		let filteredTasks = [];

		// Loops through all the tasks
		/*
		    - It's good practice to print the current element in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop
		    - Creating a separate variable to store results of array iteration methods are also good practice to avoid confusion by modifying the original array
		    - Mastering loops and arrays allow us developers to perform a wide range of features that help in data management and analysis
		*/
		allTasks.forEach(function(task) {

		    // console.log(task)

		    // If the element/string's length is greater than 10 characters
		    if(task.length > 10) {

		        // console.log(task)

		        // Add the element to the filteredTasks array
		        filteredTasks.push(task);

		    }
		});

		console.log("Result of filtered tasks:");
		console.log(filteredTasks);

		// map() 
		/* 
		    - Iterates on each element AND returns new array with different values depending on the result of the function's operation
		    - This is useful for performing tasks where mutating/changing the elements are required
		    - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
		    - Syntax
		        let/const resultArray = arrayName.map(function(indivElement))
		*/

		let numbers = [1, 2, 3, 4, 5];

		let numberMap = numbers.map(function(number) {
		    return number * number;
		});

		console.log("Result of map method:");
		console.log(numbers);

		// every() 
		/*
		    - Checks if all elements in an array meet the given condition
		    - This is useful for validating data stored in arrays especially when dealing with large amounts of data
		    - Returns a true value if all elements meet the condition and false if otherwise
		    - Syntax
		        let/const resultArray = arrayName.every(function(indivElement) {
		            return expression/condition;
		        })
		*/
		let allValid = numbers.every(function(number) {
		    return (number < 3);
		});
		console.log("Result of every method:");
		console.log(allValid);

		// some()
		/*
		    - Checks if at least one element in the array meets the given condition
		    - Returns a true value if at least one element meets the condition and false if otherwise
		    - Syntax
		        let/const resultArray = arrayName.some(function(indivElement) {
		            return expression/condition;
		        })
		*/
		let someValid = numbers.some(function(number) {
		    return (number < 2);
		});
		console.log("Result of some method:");
		console.log(someValid);

		// Combining the returned result from the every/some method may be used in other statements to perform consecutive results
		if (someValid) {
		    console.log('Some numbers in the array are greater than 2');
		};

		// filter() 
		/*
		    - Returns new array that contains elements which meets the given condition
		    - Returns an empty array if no elements were found
		    - Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
		    - Mastery of loops can help us work effectively by reducing the amount of code we use
		    - Several array iteration methods may be used to perform the same result
		    - Syntax
		        let/const resultArray = arrayName.filter(function(indivElement) {
		            return expression/condition;
		        })
		*/
		let filterValid = numbers.filter(function(number) {
		    return (number <  3);
		});
		console.log("Result of filter method:");
		console.log(filterValid);

		// No elements found
		let nothingFound = numbers.filter(function(number) {
		    return (number = 0);
		})
		console.log("Result of filter method:");
		console.log(nothingFound);

		// Filtering using foreach
		let filteredNumbers = [];

		numbers.forEach(function(number){

		    // console.log(number);

		    if(number < 3) {
		        filteredNumbers.push(number);
		    }
		})
		console.log("Result of filter method:");
		console.log(filteredNumbers);

		let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

		// The includes method
		/*
		    - Methods can be "chained" using them one after another
		    - The result of the first method is used on the second method until all "chained" methods have been resolved
		    - How chaining resolves in our example:
		        1. The "product" element will be converted into all lowercase letters
		        2. The resulting lowercased string is used in the "includes" method
		*/
		let filteredProducts = products.filter(function(product){
		    return product.toLowerCase().includes('a');
		})

		console.log(filteredProducts);

		// reduce() 
		/* 
		    - Evaluates elements from left to right and returns/reduces the array into a single value
		    - Syntax
		        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
		            return expression/operation
		        })
		    - The "accumulator" parameter in the function stores the result for every iteration of the loop
		    - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
		    - How the "reduce" method works
		        1. The first/result element in the array is stored in the "accumulator" parameter
		        2. The second/next element in the array is stored in the "currentValue" parameter
		        3. An operation is performed on the two elements
		        4. The loop repeats step 1-3 until all elements have been worked on
		*/

		let iteration = 0;

		let reducedArray = numbers.reduce(function(x, y) {
		    // Used to track the current iteration count and accumulator/currentValue data
		    console.warn('current iteration: ' + ++iteration);
		    console.log('accumulator: ' + x);
		    console.log('currentValue: ' + y);

		    // The operation to reduce the array into a single value
		    return x + y;
		});
		console.log("Result of reduce method: " + reducedArray);

		// reducing string arrays
		let list = ['Hello', 'Again', 'World'];

		let reducedJoin = list.reduce(function(x, y) {
		    return x + ' ' + y;
		});
		console.log("Result of reduce method: " + reducedJoin);

		/*
		Important Note:
			- If additional examples would be provided on Iterator methods that are not included in the session, it's recommended to focus the discussion on forEach, map, filter and reduce methods which are common array iterators that we use.
			- Refer to "references" section of this file to find the documentations for JavaScript Array forEach Method, JavaScript Array map Method, JavaScript Array every Method, JavaScript Array some Method, JavaScript Array filter Method, JavaScript String includes Method and JavaScript Reduce Method.
		*/

/*
9. Add more code to demonstrate and discuss Multidimensional arrays.
	Application > script.js
*/
		/*...*/
		console.log("Result of reduce method: " + reducedJoin);

		// [SECTION] Multidimensional Arrays
		/*
		    - Multidimensional arrays are useful for storing complex data structures
		    - A practical application of this is to help visualize/create real world objects
		    - Though useful in a number of cases, creating complex array structures is not always recommended especially since 
		*/

		let chessBoard = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
		];

		console.log(chessBoard);

		// Accessing elements of a multidimensional arrays
		console.log(chessBoard[1][4]);

		console.log("Pawn moves to: " + chessBoard[1][5]);

/*
========
Activity
========
*/

/*
Instructions that can be provided to the students for reference:

Activity References

JS Objects
https://www.w3schools.com/js/js_objects.asp
JS Object Constructor
https://www.w3schools.com/js/js_object_constructors.asp
JS Methods
https://www.w3schools.com/js/js_object_methods.asp
JS Assignment Operators
https://www.w3schools.com/js/js_assignment.asp


Activity:

Member 1:
1. In the S26 folder, create an activity folder and an index.html and index.js file inside of it.
- Create an index.html file to attach our index.js file
- Copy the template from boodle notes and paste it in an index.js file.
- Update your local sessions git repository and push to git with the commit message of Add template code s26.
- Console log the message Hello World to ensure that the script file is properly associated with the html file.
2. Create a function called addItem which is able to receive a single argument and add the input at the end of the users array.
- Function should be able to receive a single argument.
- Add the input data at the end of the array.
- The function should not be able to return data.
- Invoke and add an argument to be passed in the function.
- Log the users array in the console.
Member 2:
3. Create a function called getItemByIndex which is able to receive an index number as a single argument return the item accessed by its index.
function should be able to receive a single argument.
- return the item accessed by the index.
- Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
- log the itemFound variable in the console.

Member 3:
4. Create a function called deleteItem which is able to delete the last item in the array and return the deleted item.
- Create a function scoped variable to store the last item in the users array.
- Shorten the length of the array by at least 1 to delete the last item.
- Return the last item in the array which was stored in the function scoped variable.
- Create a global scoped variable outside of the function and store the result of the function.
- Log the global scoped variable in the console.

Member 4:
5. Create a function called updateItemByIndex which is able to update a specific item in the array by its index.
- Function should be able to receive 2 arguments, the update and the index number.
- First, access and locate the item by its index then re-assign the item with the update.
- This function should not have a return.
- Invoke the function and add the update and index number as arguments.
- Outside of the function, Log the users array in the console.
Member 5:
6. Create a function called deleteAll which is able to delete all items in the array.
- You can modify/set the length of the array.
- The function should not return anything.
- Invoke the function.
- Outside of the function, Log the users array in the console.

7. Create a function called isEmpty which is able to check if the array is empty.
- Add an if statement to check if the length of the users array is greater than 0.
- If it is, return false.
- Else, return true.
- Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
- log the isUsersEmpty variable in the console.
All Members:
8. Check out to your own git branch with git checkout -b <branchName>
9. Update your local sessions git repository and push to git with the commit message of Add activity code s26.
10. Add the sessions repo link in Boodle for s26.

*/


/* Solution: */

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    Important note: Don't pass the array as an argument to the function. The functions must be able to manipulate the current users array.
*/

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/
       
function addItem(userName){
    users[users.length] = userName;
}

addItem("John Cena");
console.log(users);

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

function getItemByIndex(index) {
    return users[index];
};

let userFound = getItemByIndex(2);
console.log(userFound);

/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

function deleteItem() {
    let deleteLastItem = users[users.length - 1];
    users.length--;
    return deleteLastItem;
}

let deletedLastItem = deleteItem();
console.log(deletedLastItem);
console.log(users);
   

/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

   
function updateItemByIndex(update, index) {
    users[index] = update;
}

updateItemByIndex("Triple H", 3);
console.log(users);

/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

function deleteAll() {
    users.length = 0
    console.log(users);
};

    
deleteAll();


/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/

function isEmpty() {
    if (users.length > 0) {
        return false
    } else {
        return true
    }
}

isUsersEmpty = isEmpty();
console.log(isUsersEmpty);



try{
    module.exports = {

        users: typeof users !== 'undefined' ? users : null,
        addItem: typeof addItem !== 'undefined' ? addItem : null,
        getItemByIndex: typeof getItemByIndex !== 'undefined' ? getItemByIndex : null,
        deleteItem: typeof deleteItem !== 'undefined' ? deleteItem : null,
        updateItemByIndex: typeof updateItemByIndex !== 'undefined' ? updateItemByIndex : null,
        deleteAll: typeof deleteAll !== 'undefined' ? deleteAll : null,
        isEmpty: typeof isEmpty !== 'undefined' ? isEmpty : null,

    }
} catch(err){

}