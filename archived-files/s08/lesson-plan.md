# WDC028 - S07 - CSS - Box Model

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1Ix_xfpjwI591qizypGjtU3yKEpDy4XYHXSPNy3LCs_c/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1Ix_xfpjwI591qizypGjtU3yKEpDy4XYHXSPNy3LCs_c/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s07) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s07/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                         | Link                                                         |
| --------------------------------------------- | ------------------------------------------------------------ |
| CSS Box Model - w3schools                     | [Link](https://www.w3schools.com/css/css_boxmodel.asp)       |
| CSS Box Model - MDN Web Docs                  | [Link](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model) |
| CSS Box Model - CSS Tricks                    | [Link](https://css-tricks.com/the-css-box-model/)            |
| Inline Blocks                                 | [Link](https://www.w3schools.com/css/css_inline-block.asp)   |
| Google Fonts - Main Page                      | [Link](https://fonts.google.com/)                            |
| Google Fonts - Open Sans                      | [Link](https://fonts.google.com/specimen/Open+Sans?query=open+sans) |
| Css Box Model Diagram - Google Chrome Browser | [Link](https://s3.amazonaws.com/viking_education/web_development/web_app_eng/css_box_model_chrome.png) |
| Css Box Model Diagram - Free Code Camp        | [Link](https://media.gcflearnfree.org/content/5ef2084faaf0ac46dc9c10be_06_23_2020/box_model.png) |
| CSS Box Sizing                                | [Link](https://www.w3schools.com/css/css3_box-sizing.asp)    |
| CSS position Property                         | [Link](https://www.w3schools.com/cssref/pr_class_position.asp) |
| CSS z-index Property                          | [Link](https://www.w3schools.com/cssref/pr_pos_z-index.asp)  |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1.[10 mins] -  Google Fonts
	2.[10 mins] -  CSS Reset
		- margin
		- padding
		- box-sizing
	3.[20 mins] -  Margin vs Padding
	4.[2 hr 20 mins] -  CSS Properties
		- padding
		- margin-bottom
		- border-bottom
		- margin
		- display
		- padding-left
		- padding-right
		- padding-top
		- padding-bottom
		- vertical-align
		- min-height
		- margin-left
		- margin-right
	5.[1 hr] -  Activity


[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. Google Chrome Browser Console
	2. Google Chrome Browser Inspect Tool
	3. CSS position property
	4. CSS z-index property

[Back to top](#table-of-contents)