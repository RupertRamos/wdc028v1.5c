# S17 - JavaScript - Basic Functions

# Session Objectives

At the end of this session, bootcampers should be able to:

- learn how to create functions in Javascript.
- learn how to call functions in Javascript.
- learn the scope of functions.
- learn about function naming conventions.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5b/-/tree/master/backend/s17)
- [Google Slide Presentation](https://docs.google.com/presentation/d/1e-GCsfEynpj26GQ25AtpQXlE6w_P1N65zIkaetc0ifc/edit#slide=id.ga6f21735ad_0_55)


## Supplemental Materials

- [Functions in Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions)
- [Function Expressions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/function)
- [Scoping](https://www.w3schools.com/js/js_scope.asp)
- [Alert Window](https://developer.mozilla.org/en-US/docs/Web/API/Window/alert)
- [Prompt Window](https://developer.mozilla.org/en-US/docs/Web/API/Window/prompt)
- [Function Naming Conventions](https://medium.com/swlh/how-to-better-name-your-functions-and-variables-e962a4ef335b)
- [Return Statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/return)
- [Return vs Console.log()](https://thecodebytes.com/difference-between-console-log-and-return-statement-in-javascript/)


# Lesson Proper

# Code Discussion

## Folder and File Preparation

Create a folder named **s17**, a folder named **discussion** inside the **s17** folder.Create a file named **index.html** and **index.js** inside the **discussion** folder.

## Basic HTML Code

Inside the **index.html** file, add the following:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Javascript - Basic Functions</title>
    </head>
    <body>
        <script src="./index.js"></script>
    </body>
</html>
```

Inside the **index.js** file, log "Hello, World!" in the console to check if the index.html file is properly connected to the index.js file.

```js
   console.log("Hello World!"); 
```

## Functions

What are functions?
- Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.
- Functions are mostly created to create complicated tasks to run several lines of code in succession.
- They are also used to prevent repeating lines/blocks of codes that perform the same task/function.

### Function Declaration

```js
	//function statement is the definition of a function (writing what the function will do). with the specified parameters.
	
	/*
	Syntax:
		function functionName() {
			code block (statement)
		}
	*/


	//function keyword - used to defined a javascript functions
	//functionName - the function name. Functions are named to be able to use later in the code.
	//function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.
````
```js
	function printName(){
	    console.log("My name is John");
	};
	
	printName();
```
### Function Invocation
	
- The statements and instructions inside a function is not immediately executed when the function is defined.
- They are run/executed when a function is invoked.
- To invoke a declared function, add the name of the function and a parenthesis.

```js
	printName();

	declaredFunction(); //results in an error, much like variables, we cannot invoke a function we have yet to define. 
```

### Function Declaration vs Function Expressions
	
Function Declaration

A function can be created through function declaration by using the function keyword and adding a function name.
Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).

```js
	//declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

	//Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

	function declaredFunction() {

		console.log("Hello World from declaredFunction()")

	}

	declaredFunction();

```

Function Expression

- A function can also be stored in a variable. This is called a function expression.
- A function expression is an anonymous function assigned to a variable.
- Anonymous function - a function without a name.
```js
	let variableFunction = function() {

		console.log("Hello Again!");

	};

	variableFunction();
```

Function Expression vs Function Declarations

Declared functions can be hoisted which mean they can be used/invoked before declaration. However, function expressions cannot be hoisted, they cannot be invoked before declaration. This ensures that the function is defined first before any invocation.
```js
	//variableFunction();
	/* 
		error - function expressions, being stored 
		in a let or const variable, cannot be hoisted.
	*/

	let variableFunction = function() {

		console.log("Hello Again!");

	};

	variableFunction();
```

We can also create a function expression of a named function.
However, to invoke the function expression, we invoke it by its variable name, not by its function name.

```js
	let funcExpression = function funcName() {

		console.log("Hello From The Other Side");

	}

	funcExpression();
```

You can reassign declared functions and function expressions to new anonymous functions.

```js
	declaredFunction = function(){
		console.log("updated declaredFunction")
	}

	declaredFunction();


	funcExpression = function (){
		console.log("updated funcExpression");
	}

	funcExpression();
```

However, we cannot re-assign a function expression initialized with const.

```js
			const constantFunc = function(){
				console.log("Initialized with const!")
			}

			constantFunc();

			constantFunc = function(){
				console.log("Cannot be re-assigned!")
			};//results in an error

			constantFunc(); 
```

### Function Scoping

Scope is the accessibility (visibility) of variables within our program.

Javascript Variables has 3 types of scope:
1. local/block scope
2. global scope
3. function scope

```js
	{
		let localVar = "Armando Perez";
	}

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);
	//console.log(localVar);//result in error. localVar, being in a block, cannot be accessed outside of its code block.
```

Function Scope

- JavaScript has function scope: Each function creates a new scope.
- Variables defined inside a function are not accessible (visible) from outside the function.
- Variables declared with var, let and const are quite similar when declared inside a function.

```js
	function showNames() {

		//Function scoped variables:
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane"; 

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);

	}

	showNames();

	console.log(functionVar);//results to an error.
	console.log(functionConst);//results to an error.
	console.log(functionLet);//results to an error.
```
The variables, functionVar,functionConst and functionLet, are function scoped and cannot be accessed outside of the function they were declared in.


Nested Functions

You can create another function inside a function. This is called a nested function. 
This nested function, being inside a parent function will have access to the variables declared in the parent function as they are within the same scope/code block.

```js
	function myNewFunction(){
		let name ="Jane";

		function nestedFunction(){
			let nestedName = "John"
			console.log(name);
		}

		//console.log(nestedName);//results to an error.
		nestedFunction();
	}

	myNewFunction();
	//nestedFunction();//results to an error. 
```

nestedName variable, being declared in the nestedFunction cannot be accessed outside of the function it was declared in.

Moreover, since this nested function is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in.

Function and Global Scoped Variables

Variables declared Globally (outside any function) have Global scope.
Global variables can be accessed from anywhere in a Javascript program including from inside a function.
That is why the nested function, myNewFunction2(), has access to the global variable, globalName.
However, nameInside is function scoped. It cannot be accessed globally or outside of the function it was declared in.

```js
	//Global Scoped Variable
	let globalName = "Alexandro";

	function myNewFunction2(){

		let nameInside = "Renz"
		console.log(globalName);

	};

	myNewFunction2();

	console.log(nameInside);//results to an error. 
```

### Return Statement

The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

returnUserName() is a function that is able to return a value which can then be saved in a variable.

```js
	function returnFullName() {

	    return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
	    console.log("This message will not be printed.");
	}
````

In our example, the "returnFullName" function was invoked/called in the same line as declaring a variable.
Whatever value is returned from the "returnFullName" function can be stored in a variable.

```js
	let fullname = returnFullName();
	console.log(fullname);
```

This way, a function is able to return a value we can further use/manipulate in our program instead of only printing/displaying it in the console.

Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.

In this example, console.log() will print the returned value of the returnFullName() function.

```js
	console.log(returnFullName());
```
You can also create a variable inside the function to contain the result and return that variable instead. You can do this for ease of use or for readability.

```js
	function returnFullAddress(){

		let fullAddress = {

			street: "#44 Maharlika St.",
			city: "Cainta",
			province: "Rizal",
		
		};

		return fullAddress;

	}

	let myAddress = returnAddress();
	console.log(myAddress);
````

On the other hand, when a function the only has console.log() to display its result it will return undefined instead:

```js
	function printPlayerInfo(){

		console.log("Username: " + "white_knight");
		console.log("Level: " + 95);
		console.log("Job: " + "Paladin");

	};

	let user1 = printPlayerInfo();
	console.log(user1);
````

returns undefined because printPlayerInfo returns nothing. It only displays the details in the console. 
You cannot save any value from printPlayerInfo() because it does not return anything.

You can return almost any data types from a function.

```js
	
	function returnSumOf5and10(){
		return 5 + 10
	}

	let sumOf5And10 = returnSumOf5and10();
	console.log(sumOf5And10);

	let total = 100 + returnSumOf5and10();
	console.log(total);


	//Simulates getting an array of user names from a database
	function getGuildMembers(){

		return ["white_knight","healer2000","masterThief100","andrewthehero","ladylady99"]

	}

	console.log(getGuildMembers());

```

#### Why Use Return?

Functions not only allow us to repeat a piece of code and display it, it can also allow us to process data and return that processed data to be further used in our program.

Unlike just using a console.log() which only displays data in the console, The return statement allows us to return data from a function which can save in a variable and further use in our program. 

Console logs are preferred to be used only for debugging but for getting or retrieving data from a source, it is preferred to use return so that we can further use this data in our program.


### Function Naming Conventions

- Function names should be definitive of the task it will perform. It usually contains a verb.

```js
	

	function getCourses(){

		let courses = ["Science 101","Math 101","English 101"];

		return courses;

	};

	let courses = getCourses();
	console.log(courses);


````
- Avoid generic names to avoid confusion within your code.

```js
	

	function get(){

		let name = "Jamie";
		return name;

	};

	let name = get();
	console.log(name);


```
- Avoid pointless and inappropriate function names.

```js
	

	function foo(){

		return 25%5;

	};

	console.log(foo());


````
- Name your functions in small caps. Follow camelCase when naming variables and functions.

```js
	

	function displayCarInfo(){

		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");

	}
	
	displayCarInfo();


```

# Activity

Note: Copy the code from activity-template.js into the batch Boodle Notes so students can copy the template of the code for this activity.

## Instructions that can be provided to the students for reference:

1. In the S17 folder, create an activity folder, an index.html file inside of it and link the index.js file.

2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.

3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.

4. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.


5. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.


6. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.


7. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.

8. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			You can also invoke the function in the console to view the returned value of the function.

			Note: This is optional.

9. Update your local backend git repository and push to git with the commit message of Add activity code s17.
10. Add the link in Boodle for s17.

Sample Output:
![readme-images/solution.png](readme-images/solution.png)
![readme-images/solution.png](readme-images/solution2.png)

## Solution

```js
/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.

*/

function getUserInfo(){

	return {

		name: "John Doe",
		age: 25,
		address: "123 Street, Quezon City",
		isMarried: false,
		petName: "Danny"

	}
}

let userInfo = getUserInfo();
console.log(userInfo);

/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.
	
*/

function getArtistsArray(){
	return ["Ben & Ben","Arthur Nery","Linkin Park","Paramore","Taylor Swift"];
}

let bandsArray = getArtistsArray();
console.log(bandsArray);


/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.
*/

function getSongsArray(){
	return ["Kathang Isip","Binhi","In the End","Bring by Boring Brick","Love Story"];
}

let songsArray = getSongsArray();
console.log(songsArray);


/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		You can also invoke the function in the console to view the returned value of the function.

		Note: This is optional.
*/

function getMoviesArray(){
	return ["The Lion King","Meet the Robinsons","Howl's Moving Castle","Tangled","Frozen"];
}

let moviesArray = getMoviesArray();
console.log(moviesArray);


/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			You can also invoke the function in the console to view the returned value of the function.

			Note: This is optional.
*/


function getPrimeNumberArray(){
	return [2,3,5,7,17]
};


let primeNumberArray = getPrimeNumberArray();
console.log(primeNumberArray)



//Do not modify
//For exporting to test.js
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){

}
```