# WDC028 - S10 - Bootstrap - Grid System

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1k1LbCbHsCr5UV-ajK38St4oeTI8cdwncSZW4RHznvH8/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1k1LbCbHsCr5UV-ajK38St4oeTI8cdwncSZW4RHznvH8/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s09) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s09/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                      | Link                                                         |
| ------------------------------------------ | ------------------------------------------------------------ |
| Bootstrap Text Utility Classes             | [Link](https://getbootstrap.com/docs/4.6/utilities/text/)    |
| Boostrap Color Utility Classes             | [Link](https://getbootstrap.com/docs/4.6/utilities/colors/)  |
| Bootstrap Border Utility Classes           | [Link](https://getbootstrap.com/docs/4.6/utilities/borders/) |
| Bootstrap Spacing Utility Classes          | [Link](https://getbootstrap.com/docs/4.6/utilities/spacing/) |
| CSS display Property                       | [Link](https://www.w3schools.com/cssref/pr_class_display.asp) |
| CSS Flexbox                                | [Link](https://www.w3schools.com/css/css3_flexbox.asp)       |
| CSS calc Function                          | [Link](https://www.w3schools.com/cssref/func_calc.asp)       |
| CSS @media Rule                            | [Link](https://www.w3schools.com/cssref/css3_pr_mediaquery.asp) |
| Bootstrap CSS Framework                    | [Link](https://getbootstrap.com/)                            |
| Introduction to Bootstrap 4.6              | [Link](https://getbootstrap.com/docs/4.6/getting-started/introduction/) |
| Bootstrap local vs. CDN                    | [Link](https://www.belugacdn.com/bootstrap-local-vs-cdn/)    |
| Responsive Meta Tag                        | [Link](https://css-tricks.com/snippets/html/responsive-meta-tag/) |
| Bootstrap Grid System (Bootstrap)          | [Link](https://getbootstrap.com/docs/4.0/layout/grid/)       |
| Bootstrap Grid System (Tutorials Republic) | [Link](https://www.tutorialrepublic.com/twitter-bootstrap-tutorial/bootstrap-grid-system.php) |
| Bootstrap Containers                       | [Link](https://getbootstrap.com/docs/4.6/layout/overview/#containers) |
| What is Mobile First Design                | [Link](https://medium.com/@Vincentxia77/what-is-mobile-first-design-why-its-important-how-to-make-it-7d3cf2e29d00) |
| Bootstrap Image Utility Classes            | [Link](https://getbootstrap.com/docs/4.6/content/images/)    |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [1hr 50 mins] - Implementing Bootstrap Grid Using CSS
	2. [10 mins] - Responsive Break Points
	3. [1 hr] - Bootstrap Grid System
		- container class
		- row class
		- column class
	4. [1 hr] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)