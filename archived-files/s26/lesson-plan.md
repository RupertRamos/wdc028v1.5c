# WDC028 - S22 - JavaScript - Array Manipulation

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Reference Material for Boodle            | [Link](#reference-material-for-boodle)            |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1n6Cl7jYEM93uxqRxnKWh_FboFfiBQdKPR3tCQc8SwO8/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1n6Cl7jYEM93uxqRxnKWh_FboFfiBQdKPR3tCQc8SwO8/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/backend/s17) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/backend/s17/Manual.js) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                   | Link                                                         |
| --------------------------------------- | ------------------------------------------------------------ |
| JavaScript Arrays (MDN Web Docs)        | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array) |
| JavaScript Arrays (W3Schools)           | [Link](https://www.w3schools.com/js/js_arrays.asp)           |
| Why Do Arrays Start With Index 0        | [Link](https://albertkoz.com/why-does-array-start-with-index-0-65ffc07cbce8) |
| JavaScript Methods                      | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions) |
| Javascript Array Methods                | [Link](https://dev.to/ibrahima92/javascript-array-methods-mutator-vs-non-mutator-15e2) |
| Mutable Definition                      | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Mutable) |
| JavaScript Array push Method            | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push) |
| JavaScript Array pop Method             | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/pop) |
| JavaScript Array unshift Method         | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift) |
| JavaScript Array shift Method           | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/shift) |
| JavaScript Array splice Method          | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) |
| JavaScript Array sort Method            | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) |
| JavaScript Array reverse Method         | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse) |
| Non-Mutator Array Methods In JavaScript | [Link](http://www.discoversdk.com/blog/non-mutator-array-methods-in-javascript) |
| JavaScript Array indexOf Method         | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf) |
| JavaScript Array lastIndexOf Method     | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf) |
| JavaScript Array slice Method           | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice) |
| JavaScript Array toString Method        | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/toString) |
| JavaScript Array concat Method          | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat) |
| JavaScript Array join Method            | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join) |
| JavaScript Array forEach Method         | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach) |
| JavaScript Array map Method             | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map) |
| JavaScript Array every Method           | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every) |
| JavaScript Array some Method            | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some) |
| JavaScript Array filter Method          | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter) |
| JavaScript String includes Method       | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes) |
| JavaScript Reduce Method                | [Link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce) |
| Callback Functions                      | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Callback_function) |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [10 mins] - Arrays and Indexes
	2. [20 mins] - Reading from array
	3. [20 mins] - Array Mutator Methods
		- push
		- pop
		- unshift
		- shift
		- splice
		- sort
		- reverse
	4. [1 hr] - Array Non-Mutator Methods
		- indexOf
		- lastIndexOf
		- slice
		- toString
		- concat
		- join
	4. [1 hr] - Array Iteration Methods
		- Foreach
		- Map
		- Every
		- Some
		- Filter
		- Reduce
	5. [10 mins] - Multi-Dimensional Arrays
	6. [1hr 30 mins] - Activity

[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

	1. Callback Functions

[Back to top](#table-of-contents)