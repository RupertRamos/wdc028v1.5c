# WDC028 - S09 - Bootstrap - Introduction and Simple Styles

## Table of Contents

| Topic                                    | Link                                              |
| ---------------------------------------- | ------------------------------------------------- |
| Standard Materials                       | [Link](#standard-materials)                       |
| Reference Materials                      | [Link](#reference-materials)                      |
| Lesson Proper                            | [Link](#lesson-proper)                            |
| Possible Relevant Topics to be Discussed | [Link](#possible-relevant-topics-to-be-discussed) |


## Standard Materials

| Resource                | Link                                                         |
| ----------------------- | ------------------------------------------------------------ |
| Objective Slide         | [Link](https://docs.google.com/presentation/d/1GH5SjdamhHIaytmP8YYJYxKWleGmFVUNIun_iFgvzPQ/edit#slide=id.g53aad6d9a4_0_1437) |
| Discussion Presentation | [Link](https://docs.google.com/presentation/d/1GH5SjdamhHIaytmP8YYJYxKWleGmFVUNIun_iFgvzPQ/edit#slide=id.g53aad6d9a4_0_728) |
| GitLab Source Code      | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/tree/main/frontend/s08) |
| Manual                  | [Link](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5a/-/blob/main/frontend/s08/Manual.html) |

[Back to top](#table-of-contents)

## Reference Materials

| Topic                                                | Link                                                         |
| ---------------------------------------------------- | ------------------------------------------------------------ |
| A Short Bootstrap Tutorial on the What, Why, and How | [Link](https://www.toptal.com/front-end/what-is-bootstrap-a-short-tutorial-on-the-what-why-and-how) |
| Bootstrap CSS Framework                              | [Link](https://getbootstrap.com/)                            |
| Introduction to Bootstrap 4.6                        | [Link](https://getbootstrap.com/docs/4.6/getting-started/introduction/) |
| Bootstrap local vs. CDN                              | [Link](https://www.belugacdn.com/bootstrap-local-vs-cdn/)    |
| Responsive Meta Tag                                  | [Link](https://css-tricks.com/snippets/html/responsive-meta-tag/) |
| Sass                                                 | [Link](https://sass-lang.com/)                               |
| Bootstrap Tables                                     | [Link](https://getbootstrap.com/docs/4.6/content/tables/)    |
| Boostrap Color Utility Classes                       | [Link](https://getbootstrap.com/docs/4.6/utilities/colors/)  |
| User Interface Design                                | [Link](https://www.interaction-design.org/literature/topics/ui-design) |
| User Experience Design                               | [Link](https://www.interaction-design.org/literature/topics/ux-design) |
| Color Theory For Designers                           | [Link](https://www.smashingmagazine.com/2010/01/color-theory-for-designers-part-1-the-meaning-of-color/) |
| Best Practices For Buttons                           | [Link](https://courseux.com/best-practices-for-buttons-the-user-experience-of-colors/) |
| Colors in UI Designers                               | [Link](https://usabilitygeek.com/colors-in-ui-design-a-guide-for-creating-the-perfect-ui/) |
| The Role of Color in UX                              | [Link](https://www.toptal.com/designers/ux/color-in-ux)      |
| Bootstrap Containers                                 | [Link](https://getbootstrap.com/docs/4.6/layout/overview/#containers) |
| Color Picker                                         | [Link](https://htmlcolorcodes.com/color-picker/)             |
| Coolors                                              | [Link](https://coolors.co/)                                  |
| Bootstrap Text Utility Classes                       | [Link](https://getbootstrap.com/docs/4.6/utilities/text/)    |
| What is Mobile First Design                          | [Link](https://medium.com/@Vincentxia77/what-is-mobile-first-design-why-its-important-how-to-make-it-7d3cf2e29d00) |
| Bootstrap Border Utility Classes                     | [Link](https://getbootstrap.com/docs/4.6/utilities/borders/) |
| Bootstrap Sizing Utility Classes                     | [Link](https://getbootstrap.com/docs/4.6/utilities/sizing/)  |
| HTML Blocks (Tutorials Point)                        | [Link](https://www.tutorialspoint.com/html/html_blocks.htm)  |
| HTML Blocks (W3Schools)                              | [Link](https://www.w3schools.com/html/html_blocks.asp)       |
| HTML Elements                                        | [Link](https://developer.mozilla.org/en-US/docs/Glossary/Element) |
| Bootstrap Position Utility Classes                   | [Link](https://getbootstrap.com/docs/4.6/utilities/position/) |
| Bootstrap Spacing Utility Classes                    | [Link](https://getbootstrap.com/docs/4.6/utilities/spacing/) |
| Bootstrap Display Utility Classes                    | [Link](https://getbootstrap.com/docs/4.6/utilities/display/) |
| Bootstrap Image Utility Classes                      | [Link](https://getbootstrap.com/docs/4.6/content/images/)    |

[Back to top](#table-of-contents)

## Lesson Proper

This outline serves as a guide for the topics discussed in class and the flow of discussion on how the materials were intended to be taught.

### Lesson Proper Outline

	1. [10 mins] - CSS Frameworks
	2. [15 mins] - CSS Bootstrap
	3. [15 mins] - Bootstrap Color Utility Classes
	4. [20 mins] - Bootstrap Text Utility Classes
	5. [20 mins] - Bootstrap Border Utility Classes
	6. [20 mins] - Bootstrap Sizing Utility Classes
	7. [20 mins] - Bootstrap Position Utility Classes
	8. [20 mins] - Bootstrap Spacing Utility Classes
	9. [20 mins] - Bootstrap Display Utility Classes
	10. [20 mins] - Bootstrap Image Utility Classes
	11. [1 hr] - Activity


[Back to top](#table-of-contents)

## Possible Relevant Topics to be Discussed

This serves as a list of topics that the instructors may use to improve the session and for better appreciation of the students. The topics listed here are topics that are not included in the materials or were only discussed in passing.

Links to external resources regarding these topics may also be found in the [Reference Materials Section](#reference-material)

### Relevant Topics

​	

[Back to top](#table-of-contents)

